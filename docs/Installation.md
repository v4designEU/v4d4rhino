# V4D4Rhino Installation

The installation of the V4D4Rhino plugin is done via Rhino's Package Manager:

1. Run the package manager command on the Rhino command line: 
- Rhino 6: `_-TestPackageManager` 
- Rhino 7: `_-PackageManager`

2. Search for "V4D4Rhino" by typing this into the package manager dialog. Click "Download and Install".

![image](https://gitlab.com/v4designEU/v4d4rhino/-/raw/main/docs/r6install.png)

3. Restart Rhino
4. Once in Rhino, click on the Panel Options gear and Select V4D4Rhino:

![image](https://gitlab.com/v4designEU/v4d4rhino/-/raw/main/docs/Panel%20Options.png)

![image](https://gitlab.com/v4designEU/v4d4rhino/-/raw/main/docs/ChooseV4D4Rhino.png)

5. You should now see the 4D4Rhino Panel displaying the login page.
6. Follow the [usage instructions](https://gitlab.com/v4designEU/v4d4rhino/-/blob/main/docs/Usage.md) to start using V4D4Rhino.