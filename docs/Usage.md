# V4D4Rhino Usage

1. Please follow the [installation instructions](https://gitlab.com/v4designEU/v4d4rhino/-/blob/main/docs/Installation.md) prior to using V4D4Rhino.
2. Login or register
3. Once authorized, you will see the main asset viewer:
![image](https://gitlab.com/v4designEU/v4d4rhino/-/raw/main/docs/v4d4rhino_assetViewer.jpg)
4. ##### Searching and Filtering:
   In the search and filter field, you can enter text to filter the assets currently in the viewer.
To search for new assets with this term, press the search button to the right of the search field.
You can further filter the assets by clicking on the filter options, and switching on one or more of the different filter switches.
![image](https://gitlab.com/v4designEU/v4d4rhino/-/raw/main/docs/filterOptions.png)
5. ##### Assets:
   The viewer shows basic and full assets. Full assets have an associated 3d model and are identified by an icon "3D" over the preview image.
![image](https://gitlab.com/v4designEU/v4d4rhino/-/raw/main/docs/Assets.png)

   Basic assets are identified by an eye icon.
   Both asset types can be bookmarked. 
   Full assets will have a download button to download the associated 3d model.
6. ##### Asset details:
   Clicking on an asset will display a detailed view of the asset.
   Basic Assets:
   ![image](https://gitlab.com/v4designEU/v4d4rhino/-/raw/main/docs/basicAssetDetails.png)
   The basic asset details dialog also allows the user to request a reconstruction of a particular shot from the asset.

   Full Assets:
   ![image](https://gitlab.com/v4designEU/v4d4rhino/-/raw/main/docs/FullAssetDetails.png)
   The full asset details dialog shows further information about the full asset and has a button for downloading the 3d model and eventually for adding that 3d model to the Rhino model.

7. ##### Creating new assets:
   ![image](https://gitlab.com/v4designEU/v4d4rhino/-/raw/main/docs/Create%20Asset.png)

   You can create a new asset from a video by clicking on the "create asset" button on the bottom of the V4D4Rhino panel and filling in the information. Once submitted, this asset will be processed by the V4Design platform and will be available as a basic asset.

