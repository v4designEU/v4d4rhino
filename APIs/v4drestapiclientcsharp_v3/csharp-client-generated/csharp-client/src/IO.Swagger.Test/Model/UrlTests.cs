/* 
 * V4DesignServer
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * 
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing Url
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class UrlTests
    {
        // TODO uncomment below to declare an instance variable for Url
        //private Url instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of Url
            //instance = new Url();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of Url
        /// </summary>
        [Test]
        public void UrlInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" Url
            //Assert.IsInstanceOfType<Url> (instance, "variable 'instance' is a Url");
        }


        /// <summary>
        /// Test the property '_Url'
        /// </summary>
        [Test]
        public void _UrlTest()
        {
            // TODO unit test for the property '_Url'
        }

    }

}
