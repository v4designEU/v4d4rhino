/* 
 * V4DesignServer
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * 
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using IO.Swagger.Client;
using IO.Swagger.Api;
using IO.Swagger.Model;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing AssetsApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class AssetsApiTests
    {
        private AssetsApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new AssetsApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of AssetsApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' AssetsApi
            //Assert.IsInstanceOfType(typeof(AssetsApi), instance, "instance is a AssetsApi");
        }

        
        /// <summary>
        /// Test AssetsCreatePost
        /// </summary>
        [Test]
        public void AssetsCreatePostTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //AssetDetails assetDetails = null;
            //var response = instance.AssetsCreatePost(assetDetails);
            //Assert.IsInstanceOf<InlineResponse2002> (response, "response is InlineResponse2002");
        }
        
        /// <summary>
        /// Test AssetsDeleteAssetIdDelete
        /// </summary>
        [Test]
        public void AssetsDeleteAssetIdDeleteTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //Object assetId = null;
            //var response = instance.AssetsDeleteAssetIdDelete(assetId);
            //Assert.IsInstanceOf<InlineResponse2001> (response, "response is InlineResponse2001");
        }
        
        /// <summary>
        /// Test AssetsGetAssetIdGet
        /// </summary>
        [Test]
        public void AssetsGetAssetIdGetTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //Object assetId = null;
            //var response = instance.AssetsGetAssetIdGet(assetId);
            //Assert.IsInstanceOf<AssetDetails> (response, "response is AssetDetails");
        }
        
        /// <summary>
        /// Test AssetsGetHistoryAssetIdGet
        /// </summary>
        [Test]
        public void AssetsGetHistoryAssetIdGetTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //Object assetId = null;
            //var response = instance.AssetsGetHistoryAssetIdGet(assetId);
            //Assert.IsInstanceOf<List<InlineResponse2003>> (response, "response is List<InlineResponse2003>");
        }
        
        /// <summary>
        /// Test AssetsImportPost
        /// </summary>
        [Test]
        public void AssetsImportPostTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //System.IO.Stream assetFile = null;
            //string assetName = null;
            //string assetDescription = null;
            //List<string> assetTags = null;
            //var response = instance.AssetsImportPost(assetFile, assetName, assetDescription, assetTags);
            //Assert.IsInstanceOf<InlineResponse2001> (response, "response is InlineResponse2001");
        }
        
        /// <summary>
        /// Test AssetsLatestPost
        /// </summary>
        [Test]
        public void AssetsLatestPostTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //SearchParams searchParams = null;
            //string type = null;
            //var response = instance.AssetsLatestPost(searchParams, type);
            //Assert.IsInstanceOf<AssetSearchResult> (response, "response is AssetSearchResult");
        }
        
        /// <summary>
        /// Test AssetsReconstructPost
        /// </summary>
        [Test]
        public void AssetsReconstructPostTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //ReconstructRequest reconstructRequest = null;
            //var response = instance.AssetsReconstructPost(reconstructRequest);
            //Assert.IsInstanceOf<InlineResponse2001> (response, "response is InlineResponse2001");
        }
        
        /// <summary>
        /// Test AssetsSearchPost
        /// </summary>
        [Test]
        public void AssetsSearchPostTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //SearchParams1 searchParams = null;
            //string type = null;
            //var response = instance.AssetsSearchPost(searchParams, type);
            //Assert.IsInstanceOf<AssetSearchResult> (response, "response is AssetSearchResult");
        }
        
        /// <summary>
        /// Test AssetsUpdateAssetIdPost
        /// </summary>
        [Test]
        public void AssetsUpdateAssetIdPostTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //Object assetId = null;
            //object assetDetails = null;
            //var response = instance.AssetsUpdateAssetIdPost(assetId, assetDetails);
            //Assert.IsInstanceOf<InlineResponse2001> (response, "response is InlineResponse2001");
        }
        
    }

}
