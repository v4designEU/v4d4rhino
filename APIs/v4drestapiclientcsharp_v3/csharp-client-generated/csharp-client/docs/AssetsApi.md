# IO.Swagger.Api.AssetsApi

All URIs are relative to *https://v4design-integration.nurogate.com/Server*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AssetsCreatePost**](AssetsApi.md#assetscreatepost) | **POST** /Assets/Create | Creates a new asset in the database
[**AssetsDeleteAssetIdDelete**](AssetsApi.md#assetsdeleteassetiddelete) | **DELETE** /Assets/Delete/{AssetId} | Delete a specific asset and all associated assets
[**AssetsGetAssetIdGet**](AssetsApi.md#assetsgetassetidget) | **GET** /Assets/Get/{AssetId} | Gets a specific asset
[**AssetsGetHistoryAssetIdGet**](AssetsApi.md#assetsgethistoryassetidget) | **GET** /Assets/GetHistory/{AssetId} | Gets the change history for an asset
[**AssetsImportPost**](AssetsApi.md#assetsimportpost) | **POST** /Assets/Import | Import a basic asset to the system
[**AssetsLatestPost**](AssetsApi.md#assetslatestpost) | **POST** /Assets/Latest | Gets a list of the latest uploaded assets
[**AssetsReconstructPost**](AssetsApi.md#assetsreconstructpost) | **POST** /Assets/Reconstruct | Instruct the reconstruction of the specified Simmo
[**AssetsSearchPost**](AssetsApi.md#assetssearchpost) | **POST** /Assets/Search | Gets a list of assets matching the specified criteria
[**AssetsUpdateAssetIdPost**](AssetsApi.md#assetsupdateassetidpost) | **POST** /Assets/Update/{AssetId} | Updates the information of an asset in the database


<a name="assetscreatepost"></a>
# **AssetsCreatePost**
> InlineResponse2002 AssetsCreatePost (AssetDetails assetDetails)

Creates a new asset in the database

Assets created using this endpoint can be updated by using the /Assets/Update/{AssetId} endpoint

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AssetsCreatePostExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new AssetsApi();
            var assetDetails = new AssetDetails(); // AssetDetails | 

            try
            {
                // Creates a new asset in the database
                InlineResponse2002 result = apiInstance.AssetsCreatePost(assetDetails);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AssetsApi.AssetsCreatePost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetDetails** | [**AssetDetails**](AssetDetails.md)|  | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="assetsdeleteassetiddelete"></a>
# **AssetsDeleteAssetIdDelete**
> InlineResponse2001 AssetsDeleteAssetIdDelete (Object assetId)

Delete a specific asset and all associated assets

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AssetsDeleteAssetIdDeleteExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new AssetsApi();
            var assetId = new Object(); // Object | 

            try
            {
                // Delete a specific asset and all associated assets
                InlineResponse2001 result = apiInstance.AssetsDeleteAssetIdDelete(assetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AssetsApi.AssetsDeleteAssetIdDelete: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | [**Object**](Object.md)|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="assetsgetassetidget"></a>
# **AssetsGetAssetIdGet**
> AssetDetails AssetsGetAssetIdGet (Object assetId)

Gets a specific asset

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AssetsGetAssetIdGetExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new AssetsApi();
            var assetId = new Object(); // Object | 

            try
            {
                // Gets a specific asset
                AssetDetails result = apiInstance.AssetsGetAssetIdGet(assetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AssetsApi.AssetsGetAssetIdGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | [**Object**](Object.md)|  | 

### Return type

[**AssetDetails**](AssetDetails.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="assetsgethistoryassetidget"></a>
# **AssetsGetHistoryAssetIdGet**
> List<InlineResponse2003> AssetsGetHistoryAssetIdGet (Object assetId)

Gets the change history for an asset

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AssetsGetHistoryAssetIdGetExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new AssetsApi();
            var assetId = new Object(); // Object | 

            try
            {
                // Gets the change history for an asset
                List&lt;InlineResponse2003&gt; result = apiInstance.AssetsGetHistoryAssetIdGet(assetId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AssetsApi.AssetsGetHistoryAssetIdGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | [**Object**](Object.md)|  | 

### Return type

[**List<InlineResponse2003>**](InlineResponse2003.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="assetsimportpost"></a>
# **AssetsImportPost**
> InlineResponse2001 AssetsImportPost (System.IO.Stream assetFile, string assetName = null, string assetDescription = null, List<string> assetTags = null)

Import a basic asset to the system

Assets will be send directly to the backend service and will only be available in the asset list and search results after processing is complete

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AssetsImportPostExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new AssetsApi();
            var assetFile = new System.IO.Stream(); // System.IO.Stream | Binary file, for example a video (maximum file size is ~250 MB)
            var assetName = assetName_example;  // string | Name of the asset (optional) 
            var assetDescription = assetDescription_example;  // string | Description of the asset (optional) 
            var assetTags = new List<string>(); // List<string> | Tags associated with the asset (optional) 

            try
            {
                // Import a basic asset to the system
                InlineResponse2001 result = apiInstance.AssetsImportPost(assetFile, assetName, assetDescription, assetTags);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AssetsApi.AssetsImportPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetFile** | **System.IO.Stream**| Binary file, for example a video (maximum file size is ~250 MB) | 
 **assetName** | **string**| Name of the asset | [optional] 
 **assetDescription** | **string**| Description of the asset | [optional] 
 **assetTags** | [**List&lt;string&gt;**](string.md)| Tags associated with the asset | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="assetslatestpost"></a>
# **AssetsLatestPost**
> AssetSearchResult AssetsLatestPost (SearchParams searchParams, string type = null)

Gets a list of the latest uploaded assets

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AssetsLatestPostExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new AssetsApi();
            var searchParams = new SearchParams(); // SearchParams | 
            var type = type_example;  // string | Only return the specified asset type, omit to retrieve all types (optional) 

            try
            {
                // Gets a list of the latest uploaded assets
                AssetSearchResult result = apiInstance.AssetsLatestPost(searchParams, type);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AssetsApi.AssetsLatestPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchParams** | [**SearchParams**](SearchParams.md)|  | 
 **type** | **string**| Only return the specified asset type, omit to retrieve all types | [optional] 

### Return type

[**AssetSearchResult**](AssetSearchResult.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="assetsreconstructpost"></a>
# **AssetsReconstructPost**
> InlineResponse2001 AssetsReconstructPost (ReconstructRequest reconstructRequest)

Instruct the reconstruction of the specified Simmo

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AssetsReconstructPostExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new AssetsApi();
            var reconstructRequest = new ReconstructRequest(); // ReconstructRequest | 

            try
            {
                // Instruct the reconstruction of the specified Simmo
                InlineResponse2001 result = apiInstance.AssetsReconstructPost(reconstructRequest);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AssetsApi.AssetsReconstructPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reconstructRequest** | [**ReconstructRequest**](ReconstructRequest.md)|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="assetssearchpost"></a>
# **AssetsSearchPost**
> AssetSearchResult AssetsSearchPost (SearchParams1 searchParams, string type = null)

Gets a list of assets matching the specified criteria

Search terms must be at least 3 characters or empty/omitted to be ignored, at least one search term must be specified. Assets must match all specified search terms to be included in the result.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AssetsSearchPostExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new AssetsApi();
            var searchParams = new SearchParams1(); // SearchParams1 | 
            var type = type_example;  // string | Only return the specified asset type, omit to retrieve all types (optional) 

            try
            {
                // Gets a list of assets matching the specified criteria
                AssetSearchResult result = apiInstance.AssetsSearchPost(searchParams, type);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AssetsApi.AssetsSearchPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **searchParams** | [**SearchParams1**](SearchParams1.md)|  | 
 **type** | **string**| Only return the specified asset type, omit to retrieve all types | [optional] 

### Return type

[**AssetSearchResult**](AssetSearchResult.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="assetsupdateassetidpost"></a>
# **AssetsUpdateAssetIdPost**
> InlineResponse2001 AssetsUpdateAssetIdPost (Object assetId, object assetDetails)

Updates the information of an asset in the database

Only assets created using the /Assets/Create endpoint can be updated

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class AssetsUpdateAssetIdPostExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new AssetsApi();
            var assetId = new Object(); // Object | 
            var assetDetails = new object(); // object | 

            try
            {
                // Updates the information of an asset in the database
                InlineResponse2001 result = apiInstance.AssetsUpdateAssetIdPost(assetId, assetDetails);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AssetsApi.AssetsUpdateAssetIdPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | [**Object**](Object.md)|  | 
 **assetDetails** | [**object**](object.md)|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

