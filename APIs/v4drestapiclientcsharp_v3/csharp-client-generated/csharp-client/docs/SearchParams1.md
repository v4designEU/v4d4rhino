# IO.Swagger.Model.SearchParams1
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Page** | **long?** | Page to display, zero or unspecified defaults to the first page (1) | [optional] [default to 0]
**Count** | **long?** | Number of results per page, zero or unspecified in interpreted as \&quot;no limit\&quot; | [optional] [default to 0]
**SearchTerm** | **string** | Search term to search for in the fields specified in \&quot;FilterBy\&quot; | [optional] [default to ""]
**FilterBy** | **List&lt;string&gt;** | The fields to search for the \&quot;SearchTerm\&quot;, empty list is interpreted as \&quot;all possible fields\&quot; | [optional] 
**Name** | **string** | Search term to search the \&quot;Name\&quot; field for | [optional] [default to ""]
**Description** | **string** | Search term to search the \&quot;Description\&quot; field for | [optional] [default to ""]
**Location** | **string** | Search term to search the \&quot;Location\&quot; field for | [optional] [default to ""]
**Style** | **string** | Search term to search the \&quot;Style\&quot; field for | [optional] [default to ""]
**Abstract** | **string** | Search term to search the \&quot;Abstract\&quot; field for | [optional] [default to ""]
**Comment** | **string** | Search term to search the \&quot;Comment\&quot; field for | [optional] [default to ""]
**Building** | **string** | Search term to search the \&quot;Building\&quot; field for | [optional] [default to ""]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

