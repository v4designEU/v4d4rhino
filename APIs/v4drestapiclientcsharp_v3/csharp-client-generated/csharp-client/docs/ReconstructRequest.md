# IO.Swagger.Model.ReconstructRequest
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Simmo** | **string** |  | 
**RepFrame** | **long?** |  | 
**FirstFrame** | **long?** |  | 
**LastFrame** | **long?** |  | 
**PreviewUrl** | **string** |  | [optional] [default to ""]
**VideoId** | **string** |  | 
**ShotIdx** | **long?** |  | 
**UserStyle** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

