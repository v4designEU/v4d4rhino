# IO.Swagger.Model.SearchParams2
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Page** | **long?** |  | 
**Count** | **long?** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

