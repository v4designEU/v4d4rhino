# IO.Swagger.Model.AssetRating
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UserId** | **string** |  | [optional] 
**Rating** | **long?** | Asset rating from 0 (lowest/worst) to 5 (highest/best), only the latest rating from a user is taken into account (NULL values are not accepted, just omit the property completely in this case) | [optional] 
**Comment** | **string** | NULL values are not accepted, just omit the property completely in this case | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

