# IO.Swagger.Model.AssetSearchResult
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TotalAmount** | **long?** |  | [optional] 
**CurrentPage** | **long?** |  | [optional] 
**TotalPages** | **long?** |  | [optional] 
**HasPreviousPage** | **bool?** |  | [optional] 
**HasNextPage** | **bool?** |  | [optional] 
**Assets** | [**List&lt;AssetDetails&gt;**](AssetDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

