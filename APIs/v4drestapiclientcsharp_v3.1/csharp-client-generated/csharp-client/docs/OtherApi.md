# IO.Swagger.Api.OtherApi

All URIs are relative to *https://v4design-integration.nurogate.com/Server*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CrawlerCrawlUrlPost**](OtherApi.md#crawlercrawlurlpost) | **POST** /Crawler/CrawlUrl | Instructs the crawler to extract data from the specified url
[**ErrorsGet**](OtherApi.md#errorsget) | **GET** /Errors | Get error information


<a name="crawlercrawlurlpost"></a>
# **CrawlerCrawlUrlPost**
> InlineResponse2001 CrawlerCrawlUrlPost (Url url)

Instructs the crawler to extract data from the specified url

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class CrawlerCrawlUrlPostExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new OtherApi();
            var url = new Url(); // Url | 

            try
            {
                // Instructs the crawler to extract data from the specified url
                InlineResponse2001 result = apiInstance.CrawlerCrawlUrlPost(url);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OtherApi.CrawlerCrawlUrlPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **url** | [**Url**](Url.md)|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="errorsget"></a>
# **ErrorsGet**
> void ErrorsGet ()

Get error information

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ErrorsGetExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new OtherApi();

            try
            {
                // Get error information
                apiInstance.ErrorsGet();
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling OtherApi.ErrorsGet: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

