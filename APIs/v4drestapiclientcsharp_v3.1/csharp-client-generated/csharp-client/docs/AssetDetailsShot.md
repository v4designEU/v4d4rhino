# IO.Swagger.Model.AssetDetailsShot
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RepFrame** | **string** |  | [optional] 
**Simmo** | **string** |  | [optional] 
**FirstFrame** | **string** |  | [optional] 
**LastFrame** | **string** |  | [optional] 
**PreviewUrl** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

