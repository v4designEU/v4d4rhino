# IO.Swagger.Api.UserApi

All URIs are relative to *https://v4design-integration.nurogate.com/Server*

Method | HTTP request | Description
------------- | ------------- | -------------
[**SessionDestroyPost**](UserApi.md#sessiondestroypost) | **POST** /Session/Destroy | Ends a running session before it is expired
[**SessionRefreshPost**](UserApi.md#sessionrefreshpost) | **POST** /Session/Refresh | Refreshes a running session to extend its expiration time
[**UserCreatePost**](UserApi.md#usercreatepost) | **POST** /User/Create | Creates a new user
[**UserGetUserIdGet**](UserApi.md#usergetuseridget) | **GET** /User/Get/{UserId} | Gets information of an existing user
[**UserLoginPost**](UserApi.md#userloginpost) | **POST** /User/Login | Logs the user in and creates a session
[**UserUpdateUserIdPatch**](UserApi.md#userupdateuseridpatch) | **PATCH** /User/Update/{UserId} | Updates the information of an existing user


<a name="sessiondestroypost"></a>
# **SessionDestroyPost**
> InlineResponse2001 SessionDestroyPost ()

Ends a running session before it is expired

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class SessionDestroyPostExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new UserApi();

            try
            {
                // Ends a running session before it is expired
                InlineResponse2001 result = apiInstance.SessionDestroyPost();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UserApi.SessionDestroyPost: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="sessionrefreshpost"></a>
# **SessionRefreshPost**
> InlineResponse200 SessionRefreshPost ()

Refreshes a running session to extend its expiration time

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class SessionRefreshPostExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new UserApi();

            try
            {
                // Refreshes a running session to extend its expiration time
                InlineResponse200 result = apiInstance.SessionRefreshPost();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UserApi.SessionRefreshPost: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="usercreatepost"></a>
# **UserCreatePost**
> InlineResponse200 UserCreatePost (UserDetails1 userDetails)

Creates a new user

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UserCreatePostExample
    {
        public void main()
        {
            var apiInstance = new UserApi();
            var userDetails = new UserDetails1(); // UserDetails1 | 

            try
            {
                // Creates a new user
                InlineResponse200 result = apiInstance.UserCreatePost(userDetails);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UserApi.UserCreatePost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userDetails** | [**UserDetails1**](UserDetails1.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="usergetuseridget"></a>
# **UserGetUserIdGet**
> UserDetails UserGetUserIdGet (Object userId)

Gets information of an existing user

Special UserId 'Me' can be used to access details of currently logged in user

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UserGetUserIdGetExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new UserApi();
            var userId = new Object(); // Object | 

            try
            {
                // Gets information of an existing user
                UserDetails result = apiInstance.UserGetUserIdGet(userId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UserApi.UserGetUserIdGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | [**Object**](Object.md)|  | 

### Return type

[**UserDetails**](UserDetails.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="userloginpost"></a>
# **UserLoginPost**
> InlineResponse200 UserLoginPost (UserDetails2 userDetails)

Logs the user in and creates a session

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UserLoginPostExample
    {
        public void main()
        {
            var apiInstance = new UserApi();
            var userDetails = new UserDetails2(); // UserDetails2 | 

            try
            {
                // Logs the user in and creates a session
                InlineResponse200 result = apiInstance.UserLoginPost(userDetails);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UserApi.UserLoginPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userDetails** | [**UserDetails2**](UserDetails2.md)|  | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="userupdateuseridpatch"></a>
# **UserUpdateUserIdPatch**
> UserDetails UserUpdateUserIdPatch (Object userId, object userDetails)

Updates the information of an existing user

Special UserId 'Me' can be used to update details of currently logged in user

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class UserUpdateUserIdPatchExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new UserApi();
            var userId = new Object(); // Object | 
            var userDetails = new object(); // object | 

            try
            {
                // Updates the information of an existing user
                UserDetails result = apiInstance.UserUpdateUserIdPatch(userId, userDetails);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UserApi.UserUpdateUserIdPatch: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | [**Object**](Object.md)|  | 
 **userDetails** | [**object**](object.md)|  | 

### Return type

[**UserDetails**](UserDetails.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

