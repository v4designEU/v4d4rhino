# IO.Swagger.Model.SearchParams
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Page** | **long?** | Page to display, zero or unspecified defaults to the first page (1) | [optional] [default to 0]
**Count** | **long?** | Number of results per page, zero or unspecified in interpreted as \&quot;no limit\&quot; | [optional] [default to 0]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

