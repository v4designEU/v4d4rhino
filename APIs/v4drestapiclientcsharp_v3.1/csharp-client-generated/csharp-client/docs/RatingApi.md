# IO.Swagger.Api.RatingApi

All URIs are relative to *https://v4design-integration.nurogate.com/Server*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RatingGetAssetIdPost**](RatingApi.md#ratinggetassetidpost) | **POST** /Rating/Get/{AssetId} | Gets the ratings for an asset
[**RatingRateAssetIdPost**](RatingApi.md#ratingrateassetidpost) | **POST** /Rating/Rate/{AssetId} | Adds a rating to an asset
[**RatingRatingIdDelete**](RatingApi.md#ratingratingiddelete) | **DELETE** /Rating/{RatingId} | Delete the specified rating (admins only)


<a name="ratinggetassetidpost"></a>
# **RatingGetAssetIdPost**
> AssetRatingSearchResult RatingGetAssetIdPost (Object assetId, SearchParams2 searchParams)

Gets the ratings for an asset

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class RatingGetAssetIdPostExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new RatingApi();
            var assetId = new Object(); // Object | 
            var searchParams = new SearchParams2(); // SearchParams2 | 

            try
            {
                // Gets the ratings for an asset
                AssetRatingSearchResult result = apiInstance.RatingGetAssetIdPost(assetId, searchParams);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RatingApi.RatingGetAssetIdPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | [**Object**](Object.md)|  | 
 **searchParams** | [**SearchParams2**](SearchParams2.md)|  | 

### Return type

[**AssetRatingSearchResult**](AssetRatingSearchResult.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="ratingrateassetidpost"></a>
# **RatingRateAssetIdPost**
> InlineResponse2001 RatingRateAssetIdPost (Object assetId, AssetRating rating)

Adds a rating to an asset

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class RatingRateAssetIdPostExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new RatingApi();
            var assetId = new Object(); // Object | 
            var rating = new AssetRating(); // AssetRating | 

            try
            {
                // Adds a rating to an asset
                InlineResponse2001 result = apiInstance.RatingRateAssetIdPost(assetId, rating);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RatingApi.RatingRateAssetIdPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | [**Object**](Object.md)|  | 
 **rating** | [**AssetRating**](AssetRating.md)|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="ratingratingiddelete"></a>
# **RatingRatingIdDelete**
> InlineResponse2001 RatingRatingIdDelete (Object ratingId)

Delete the specified rating (admins only)

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class RatingRatingIdDeleteExample
    {
        public void main()
        {
            // Configure API key authorization: SessionId
            Configuration.Default.AddApiKey("X-V4DesignSession", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // Configuration.Default.AddApiKeyPrefix("X-V4DesignSession", "Bearer");

            var apiInstance = new RatingApi();
            var ratingId = new Object(); // Object | 

            try
            {
                // Delete the specified rating (admins only)
                InlineResponse2001 result = apiInstance.RatingRatingIdDelete(ratingId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RatingApi.RatingRatingIdDelete: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ratingId** | [**Object**](Object.md)|  | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[SessionId](../README.md#SessionId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

