# IO.Swagger.Model.AssetRatingSearchResult
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Average** | **float?** | The average rating of the asset | [optional] 
**EffectiveRatings** | **long?** | The total number of ratings that have been taken into account for the average rating of the asset | [optional] 
**TotalAmount** | **long?** | The total number of comments or ratings | [optional] 
**Ratings** | [**List&lt;AssetRating&gt;**](AssetRating.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

