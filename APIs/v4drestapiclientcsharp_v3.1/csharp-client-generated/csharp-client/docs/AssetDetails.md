# IO.Swagger.Model.AssetDetails
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AssetId** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**DownloadUrl** | **string** |  | [optional] 
**ThumbnailUrl** | **string** |  | [optional] 
**_3DModelThumbnailUrl** | **string** |  | [optional] 
**Description** | **List&lt;Object&gt;** |  | [optional] 
**Latitude** | **float?** |  | [optional] 
**Longitude** | **float?** |  | [optional] 
**Tags** | **List&lt;string&gt;** |  | [optional] 
**Location** | **string** |  | [optional] 
**License** | **List&lt;string&gt;** |  | [optional] 
**Source** | **List&lt;string&gt;** |  | [optional] 
**PublicationDate** | **List&lt;string&gt;** |  | [optional] 
**ReconstructionDate** | **string** |  | [optional] 
**BasicAssets** | **List&lt;string&gt;** |  | [optional] 
**ShotIds** | **List&lt;string&gt;** |  | [optional] 
**Textures** | [**List&lt;AssetDetailsTexture&gt;**](AssetDetailsTexture.md) |  | [optional] 
**Shots** | [**List&lt;AssetDetailsShot&gt;**](AssetDetailsShot.md) |  | [optional] 
**KBID** | **string** |  | [optional] 
**LatestRatings** | [**AssetDetailsLatestRatings**](AssetDetailsLatestRatings.md) |  | [optional] 
**Errors** | **List&lt;Object&gt;** |  | [optional] 
**Simmo** | **List&lt;string&gt;** |  | [optional] 
**BIM** | **List&lt;Object&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

