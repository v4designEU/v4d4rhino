# IO.Swagger.Model.AssetDetailsTexture
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ThumbnailUrl** | **string** |  | [optional] 
**FileUrl** | **string** |  | [optional] 
**Style** | **string** |  | [optional] 
**Emotion** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

