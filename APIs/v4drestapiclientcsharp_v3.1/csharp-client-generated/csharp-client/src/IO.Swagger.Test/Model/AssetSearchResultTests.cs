/* 
 * V4DesignServer
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * 
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing AssetSearchResult
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class AssetSearchResultTests
    {
        // TODO uncomment below to declare an instance variable for AssetSearchResult
        //private AssetSearchResult instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of AssetSearchResult
            //instance = new AssetSearchResult();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of AssetSearchResult
        /// </summary>
        [Test]
        public void AssetSearchResultInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" AssetSearchResult
            //Assert.IsInstanceOfType<AssetSearchResult> (instance, "variable 'instance' is a AssetSearchResult");
        }


        /// <summary>
        /// Test the property 'TotalAmount'
        /// </summary>
        [Test]
        public void TotalAmountTest()
        {
            // TODO unit test for the property 'TotalAmount'
        }
        /// <summary>
        /// Test the property 'CurrentPage'
        /// </summary>
        [Test]
        public void CurrentPageTest()
        {
            // TODO unit test for the property 'CurrentPage'
        }
        /// <summary>
        /// Test the property 'TotalPages'
        /// </summary>
        [Test]
        public void TotalPagesTest()
        {
            // TODO unit test for the property 'TotalPages'
        }
        /// <summary>
        /// Test the property 'HasPreviousPage'
        /// </summary>
        [Test]
        public void HasPreviousPageTest()
        {
            // TODO unit test for the property 'HasPreviousPage'
        }
        /// <summary>
        /// Test the property 'HasNextPage'
        /// </summary>
        [Test]
        public void HasNextPageTest()
        {
            // TODO unit test for the property 'HasNextPage'
        }
        /// <summary>
        /// Test the property 'Assets'
        /// </summary>
        [Test]
        public void AssetsTest()
        {
            // TODO unit test for the property 'Assets'
        }

    }

}
