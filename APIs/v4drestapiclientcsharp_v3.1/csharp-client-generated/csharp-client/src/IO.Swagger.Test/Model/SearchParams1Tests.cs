/* 
 * V4DesignServer
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * 
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing SearchParams1
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class SearchParams1Tests
    {
        // TODO uncomment below to declare an instance variable for SearchParams1
        //private SearchParams1 instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of SearchParams1
            //instance = new SearchParams1();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of SearchParams1
        /// </summary>
        [Test]
        public void SearchParams1InstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" SearchParams1
            //Assert.IsInstanceOfType<SearchParams1> (instance, "variable 'instance' is a SearchParams1");
        }


        /// <summary>
        /// Test the property 'Page'
        /// </summary>
        [Test]
        public void PageTest()
        {
            // TODO unit test for the property 'Page'
        }
        /// <summary>
        /// Test the property 'Count'
        /// </summary>
        [Test]
        public void CountTest()
        {
            // TODO unit test for the property 'Count'
        }
        /// <summary>
        /// Test the property 'SearchTerm'
        /// </summary>
        [Test]
        public void SearchTermTest()
        {
            // TODO unit test for the property 'SearchTerm'
        }
        /// <summary>
        /// Test the property 'FilterBy'
        /// </summary>
        [Test]
        public void FilterByTest()
        {
            // TODO unit test for the property 'FilterBy'
        }
        /// <summary>
        /// Test the property 'Name'
        /// </summary>
        [Test]
        public void NameTest()
        {
            // TODO unit test for the property 'Name'
        }
        /// <summary>
        /// Test the property 'Description'
        /// </summary>
        [Test]
        public void DescriptionTest()
        {
            // TODO unit test for the property 'Description'
        }
        /// <summary>
        /// Test the property 'Location'
        /// </summary>
        [Test]
        public void LocationTest()
        {
            // TODO unit test for the property 'Location'
        }
        /// <summary>
        /// Test the property 'Style'
        /// </summary>
        [Test]
        public void StyleTest()
        {
            // TODO unit test for the property 'Style'
        }
        /// <summary>
        /// Test the property 'Abstract'
        /// </summary>
        [Test]
        public void AbstractTest()
        {
            // TODO unit test for the property 'Abstract'
        }
        /// <summary>
        /// Test the property 'Comment'
        /// </summary>
        [Test]
        public void CommentTest()
        {
            // TODO unit test for the property 'Comment'
        }
        /// <summary>
        /// Test the property 'Building'
        /// </summary>
        [Test]
        public void BuildingTest()
        {
            // TODO unit test for the property 'Building'
        }

    }

}
