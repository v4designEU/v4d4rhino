# IO.Swagger.Model.SegmentationResult
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ZipUrl** | **string** |  | [optional] 
**ModelId** | **string** |  | [optional] 
**SegmentedObj** | **string** |  | [optional] 
**OriginalObj** | **string** |  | [optional] 
**RhinoProject** | **string** |  | [optional] 
**Ttl** | **string** |  | [optional] 
**Status** | **string** |  | [optional] 
**Process** | **double?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

