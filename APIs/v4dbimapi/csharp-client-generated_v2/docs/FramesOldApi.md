# IO.Swagger.Api.FramesOldApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**TestFrameGetjpgGet**](FramesOldApi.md#testframegetjpgget) | **GET** /TestFrameGetjpg | 

<a name="testframegetjpgget"></a>
# **TestFrameGetjpgGet**
> void TestFrameGetjpgGet (string simmo = null, int? idx = null)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class TestFrameGetjpgGetExample
    {
        public void main()
        {
            var apiInstance = new FramesOldApi();
            var simmo = simmo_example;  // string |  (optional) 
            var idx = 56;  // int? |  (optional) 

            try
            {
                apiInstance.TestFrameGetjpgGet(simmo, idx);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FramesOldApi.TestFrameGetjpgGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simmo** | **string**|  | [optional] 
 **idx** | **int?**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
