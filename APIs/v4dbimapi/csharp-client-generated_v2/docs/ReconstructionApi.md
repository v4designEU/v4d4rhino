# IO.Swagger.Api.ReconstructionApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ApiReconstructionGetStyleListGet**](ReconstructionApi.md#apireconstructiongetstylelistget) | **GET** /api/Reconstruction/GetStyleList | 
[**ApiReconstructionRenderFramePosGet**](ReconstructionApi.md#apireconstructionrenderframeposget) | **GET** /api/Reconstruction/RenderFramePos | 
[**ApiReconstructionStartStyleTransferGet**](ReconstructionApi.md#apireconstructionstartstyletransferget) | **GET** /api/Reconstruction/StartStyleTransfer | 
[**ApiReconstructionStartStyleTransferPost**](ReconstructionApi.md#apireconstructionstartstyletransferpost) | **POST** /api/Reconstruction/StartStyleTransfer | 

<a name="apireconstructiongetstylelistget"></a>
# **ApiReconstructionGetStyleListGet**
> void ApiReconstructionGetStyleListGet ()



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiReconstructionGetStyleListGetExample
    {
        public void main()
        {
            var apiInstance = new ReconstructionApi();

            try
            {
                apiInstance.ApiReconstructionGetStyleListGet();
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReconstructionApi.ApiReconstructionGetStyleListGet: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apireconstructionrenderframeposget"></a>
# **ApiReconstructionRenderFramePosGet**
> void ApiReconstructionRenderFramePosGet (string id = null, int? idx = null)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiReconstructionRenderFramePosGetExample
    {
        public void main()
        {
            var apiInstance = new ReconstructionApi();
            var id = id_example;  // string |  (optional) 
            var idx = 56;  // int? |  (optional) 

            try
            {
                apiInstance.ApiReconstructionRenderFramePosGet(id, idx);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReconstructionApi.ApiReconstructionRenderFramePosGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  | [optional] 
 **idx** | **int?**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apireconstructionstartstyletransferget"></a>
# **ApiReconstructionStartStyleTransferGet**
> void ApiReconstructionStartStyleTransferGet (StyleRequest body = null)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiReconstructionStartStyleTransferGetExample
    {
        public void main()
        {
            var apiInstance = new ReconstructionApi();
            var body = new StyleRequest(); // StyleRequest |  (optional) 

            try
            {
                apiInstance.ApiReconstructionStartStyleTransferGet(body);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReconstructionApi.ApiReconstructionStartStyleTransferGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**StyleRequest**](StyleRequest.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apireconstructionstartstyletransferpost"></a>
# **ApiReconstructionStartStyleTransferPost**
> void ApiReconstructionStartStyleTransferPost (StyleRequest body = null)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiReconstructionStartStyleTransferPostExample
    {
        public void main()
        {
            var apiInstance = new ReconstructionApi();
            var body = new StyleRequest(); // StyleRequest |  (optional) 

            try
            {
                apiInstance.ApiReconstructionStartStyleTransferPost(body);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ReconstructionApi.ApiReconstructionStartStyleTransferPost: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**StyleRequest**](StyleRequest.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/_*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
