# IO.Swagger.Api.BIMApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ApiBIMGetFileIdGet**](BIMApi.md#apibimgetfileidget) | **GET** /api/BIM/GetFile/{id} | 
[**ApiBIMGetReconstructionByNameGet**](BIMApi.md#apibimgetreconstructionbynameget) | **GET** /api/BIM/GetReconstructionByName | 
[**ApiBIMGetSegmentationGet**](BIMApi.md#apibimgetsegmentationget) | **GET** /api/BIM/GetSegmentation | Gets a previously segmented model. \\n to be implemented
[**ApiBIMGetTtlGet**](BIMApi.md#apibimgetttlget) | **GET** /api/BIM/GetTtl | Get the ttl file for a previously segmented model
[**ApiBIMStartSegmentationGet**](BIMApi.md#apibimstartsegmentationget) | **GET** /api/BIM/StartSegmentation | Start segmentation process

<a name="apibimgetfileidget"></a>
# **ApiBIMGetFileIdGet**
> void ApiBIMGetFileIdGet (int? id)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiBIMGetFileIdGetExample
    {
        public void main()
        {
            var apiInstance = new BIMApi();
            var id = 56;  // int? | 

            try
            {
                apiInstance.ApiBIMGetFileIdGet(id);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BIMApi.ApiBIMGetFileIdGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int?**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apibimgetreconstructionbynameget"></a>
# **ApiBIMGetReconstructionByNameGet**
> void ApiBIMGetReconstructionByNameGet (string name = null)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiBIMGetReconstructionByNameGetExample
    {
        public void main()
        {
            var apiInstance = new BIMApi();
            var name = name_example;  // string |  (optional) 

            try
            {
                apiInstance.ApiBIMGetReconstructionByNameGet(name);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BIMApi.ApiBIMGetReconstructionByNameGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apibimgetsegmentationget"></a>
# **ApiBIMGetSegmentationGet**
> SegmentationResult ApiBIMGetSegmentationGet (string modelid = null)

Gets a previously segmented model. \\n to be implemented

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiBIMGetSegmentationGetExample
    {
        public void main()
        {
            var apiInstance = new BIMApi();
            var modelid = modelid_example;  // string |  (optional) 

            try
            {
                // Gets a previously segmented model. \\n to be implemented
                SegmentationResult result = apiInstance.ApiBIMGetSegmentationGet(modelid);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BIMApi.ApiBIMGetSegmentationGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **modelid** | **string**|  | [optional] 

### Return type

[**SegmentationResult**](SegmentationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apibimgetttlget"></a>
# **ApiBIMGetTtlGet**
> void ApiBIMGetTtlGet (string modelid = null)

Get the ttl file for a previously segmented model

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiBIMGetTtlGetExample
    {
        public void main()
        {
            var apiInstance = new BIMApi();
            var modelid = modelid_example;  // string |  (optional) 

            try
            {
                // Get the ttl file for a previously segmented model
                apiInstance.ApiBIMGetTtlGet(modelid);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BIMApi.ApiBIMGetTtlGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **modelid** | **string**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apibimstartsegmentationget"></a>
# **ApiBIMStartSegmentationGet**
> SegmentationResult ApiBIMStartSegmentationGet (string kbId = null)

Start segmentation process

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiBIMStartSegmentationGetExample
    {
        public void main()
        {
            var apiInstance = new BIMApi();
            var kbId = kbId_example;  // string | the id of the model (optional) 

            try
            {
                // Start segmentation process
                SegmentationResult result = apiInstance.ApiBIMStartSegmentationGet(kbId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling BIMApi.ApiBIMStartSegmentationGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **kbId** | **string**| the id of the model | [optional] 

### Return type

[**SegmentationResult**](SegmentationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
