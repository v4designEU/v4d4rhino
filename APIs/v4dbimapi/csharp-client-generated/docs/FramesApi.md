# IO.Swagger.Api.FramesApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ApiFramesSimmoIdxDlFormatGet**](FramesApi.md#apiframessimmoidxdlformatget) | **GET** /api/Frames/{simmo}/{idx}/dl/{format} | Gets a 0-based frame of video. request frames IN ORDER cause video is decoded during request. lossless quality &#x3D; webp(3)
[**ApiFramesTestFrameGetMaskGet**](FramesApi.md#apiframestestframegetmaskget) | **GET** /api/Frames/TestFrameGetMask | 
[**ApiFramesTestFrameGetWebpGet**](FramesApi.md#apiframestestframegetwebpget) | **GET** /api/Frames/TestFrameGetWebp | 
[**ApiFramesTestFrameGetjpgGet**](FramesApi.md#apiframestestframegetjpgget) | **GET** /api/Frames/TestFrameGetjpg | 
[**ApiFramesTestFrameGetpngGet**](FramesApi.md#apiframestestframegetpngget) | **GET** /api/Frames/TestFrameGetpng | 
[**ApiFramesTestReconSimmoGet**](FramesApi.md#apiframestestreconsimmoget) | **GET** /api/Frames/TestReconSimmo | Gets a 0-based frame of video.
[**ApiFramesTestShotsSimmoGet**](FramesApi.md#apiframestestshotssimmoget) | **GET** /api/Frames/TestShotsSimmo | 

<a name="apiframessimmoidxdlformatget"></a>
# **ApiFramesSimmoIdxDlFormatGet**
> void ApiFramesSimmoIdxDlFormatGet (string simmo, int? idx, ImageFormat format)

Gets a 0-based frame of video. request frames IN ORDER cause video is decoded during request. lossless quality = webp(3)

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiFramesSimmoIdxDlFormatGetExample
    {
        public void main()
        {
            var apiInstance = new FramesApi();
            var simmo = simmo_example;  // string | 
            var idx = 56;  // int? | 
            var format = new ImageFormat(); // ImageFormat | 0 = JPG, 1 = WEBP, 2 = BMP, 3 = PNG

            try
            {
                // Gets a 0-based frame of video. request frames IN ORDER cause video is decoded during request. lossless quality = webp(3)
                apiInstance.ApiFramesSimmoIdxDlFormatGet(simmo, idx, format);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FramesApi.ApiFramesSimmoIdxDlFormatGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simmo** | **string**|  | 
 **idx** | **int?**|  | 
 **format** | [**ImageFormat**](ImageFormat.md)| 0 &#x3D; JPG, 1 &#x3D; WEBP, 2 &#x3D; BMP, 3 &#x3D; PNG | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apiframestestframegetmaskget"></a>
# **ApiFramesTestFrameGetMaskGet**
> void ApiFramesTestFrameGetMaskGet (string simmo = null, int? idx = null)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiFramesTestFrameGetMaskGetExample
    {
        public void main()
        {
            var apiInstance = new FramesApi();
            var simmo = simmo_example;  // string |  (optional) 
            var idx = 56;  // int? |  (optional) 

            try
            {
                apiInstance.ApiFramesTestFrameGetMaskGet(simmo, idx);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FramesApi.ApiFramesTestFrameGetMaskGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simmo** | **string**|  | [optional] 
 **idx** | **int?**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apiframestestframegetwebpget"></a>
# **ApiFramesTestFrameGetWebpGet**
> void ApiFramesTestFrameGetWebpGet (string simmo = null, int? idx = null)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiFramesTestFrameGetWebpGetExample
    {
        public void main()
        {
            var apiInstance = new FramesApi();
            var simmo = simmo_example;  // string |  (optional) 
            var idx = 56;  // int? |  (optional) 

            try
            {
                apiInstance.ApiFramesTestFrameGetWebpGet(simmo, idx);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FramesApi.ApiFramesTestFrameGetWebpGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simmo** | **string**|  | [optional] 
 **idx** | **int?**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apiframestestframegetjpgget"></a>
# **ApiFramesTestFrameGetjpgGet**
> void ApiFramesTestFrameGetjpgGet (string simmo = null, int? idx = null)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiFramesTestFrameGetjpgGetExample
    {
        public void main()
        {
            var apiInstance = new FramesApi();
            var simmo = simmo_example;  // string |  (optional) 
            var idx = 56;  // int? |  (optional) 

            try
            {
                apiInstance.ApiFramesTestFrameGetjpgGet(simmo, idx);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FramesApi.ApiFramesTestFrameGetjpgGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simmo** | **string**|  | [optional] 
 **idx** | **int?**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apiframestestframegetpngget"></a>
# **ApiFramesTestFrameGetpngGet**
> void ApiFramesTestFrameGetpngGet (string simmo = null, int? idx = null)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiFramesTestFrameGetpngGetExample
    {
        public void main()
        {
            var apiInstance = new FramesApi();
            var simmo = simmo_example;  // string |  (optional) 
            var idx = 56;  // int? |  (optional) 

            try
            {
                apiInstance.ApiFramesTestFrameGetpngGet(simmo, idx);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FramesApi.ApiFramesTestFrameGetpngGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simmo** | **string**|  | [optional] 
 **idx** | **int?**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apiframestestreconsimmoget"></a>
# **ApiFramesTestReconSimmoGet**
> void ApiFramesTestReconSimmoGet (string simmo = null)

Gets a 0-based frame of video.

### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiFramesTestReconSimmoGetExample
    {
        public void main()
        {
            var apiInstance = new FramesApi();
            var simmo = simmo_example;  // string |  (optional) 

            try
            {
                // Gets a 0-based frame of video.
                apiInstance.ApiFramesTestReconSimmoGet(simmo);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FramesApi.ApiFramesTestReconSimmoGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simmo** | **string**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
<a name="apiframestestshotssimmoget"></a>
# **ApiFramesTestShotsSimmoGet**
> void ApiFramesTestShotsSimmoGet (string simmo = null)



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiFramesTestShotsSimmoGetExample
    {
        public void main()
        {
            var apiInstance = new FramesApi();
            var simmo = simmo_example;  // string |  (optional) 

            try
            {
                apiInstance.ApiFramesTestShotsSimmoGet(simmo);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FramesApi.ApiFramesTestShotsSimmoGet: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **simmo** | **string**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
