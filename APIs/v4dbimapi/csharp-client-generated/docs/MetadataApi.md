# IO.Swagger.Api.MetadataApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ApiMetadataGetMasksGet**](MetadataApi.md#apimetadatagetmasksget) | **GET** /api/Metadata/getMasks | 

<a name="apimetadatagetmasksget"></a>
# **ApiMetadataGetMasksGet**
> void ApiMetadataGetMasksGet ()



### Example
```csharp
using System;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;

namespace Example
{
    public class ApiMetadataGetMasksGetExample
    {
        public void main()
        {
            var apiInstance = new MetadataApi();

            try
            {
                apiInstance.ApiMetadataGetMasksGet();
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MetadataApi.ApiMetadataGetMasksGet: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
