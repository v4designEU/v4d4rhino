﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

namespace V4D4Rhino
{
    public class V4DAsset
    {
        /// <summary>
        /// The URL for the asset thumbnail
        /// </summary>
        public string ThumbnailURL { get; set; }

        /// <summary>
        /// The URL for the asset 3d model
        /// </summary>
        public byte[] ModelURL { get; set; }

        /// <summary>
        /// The name of the asset.
        /// </summary>
        public byte[] Name { get; set; }

        /// <summary>
        /// Description of the asset.
        /// </summary>
        public byte[] Description { get; set; }

        /// <summary>
        /// The number of vertices in the model.
        /// </summary>
        public int VertexCount { get; set; }

        /// <summary>
        /// The number of vertices in the model.
        /// </summary>
        public int TextureCount { get; set; }

        /// <summary>
        /// The number of faces in the model.
        /// </summary>
        public int FaceCount { get; set; }

        /// <summary>
        /// The location of the model
        /// </summary>
        public byte[] Location { get; internal set; }

        /// <summary>
        /// The source of the artifact.
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// The nickname of the artifact.
        /// </summary>
        public byte[] Nickname { get; set; }

        /// <summary>
        /// The creator of the artifact.
        /// </summary>
        public byte[] Creator { get; set; }

        /// <summary>
        /// The style of the artifact.
        /// </summary>
        public byte[] Style { get; set; }

        /// <summary>
        /// The building associated with this artifact.
        /// </summary>
        public byte[] Building { get; set; }

        /// <summary>
        /// The person associated with this artifact.
        /// </summary>
        public byte[] Person { get; set; }

        /// <summary>
        /// The organization associated with this artifact.
        /// </summary>
        public byte[] Organization { get; set; }


        /// <summary>
        /// The year of the artifact.
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// A list of tags
        /// </summary>
        public List<string> Tags { get; set; }

        /// <summary>
        /// The asset Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The Knowledge Base Id of this asset.
        /// </summary>
        public string KbId { get; set; }

        /// <summary>
        /// The size of the archive.
        /// </summary>
        public string Size { get; set; }

        /// <summary>
        /// List of rhino Object Ids associated with this asset. 
        /// Is populated once the asset is added to Rhino.
        /// </summary>
        public List<Guid> RhinoObjectIds { get; set; }

        /// <summary>
        /// Local path to asset files.
        /// </summary>
        public string LocalDirectory { get; set; }

        /// <summary>
        /// List of texture directories if they exist.
        /// </summary>
        public List<byte []> TextureDirectories { get; set; }

        /// <summary>
        /// This flag is set after the asset as been unzipped and the contents can be interrogated.
        /// </summary>
        public bool HasOptionalTextures { get; set; }

        /// <summary>
        /// Convert this object to a JSON string
        /// </summary>
        /// <returns>JSON string</returns>
        public string ToJSON()
        {

            var settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Formatting = Formatting.None
            };

            return JsonConvert.SerializeObject(this, settings);
        }

    }
}
