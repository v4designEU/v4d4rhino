﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using Rhino;

namespace V4D4Rhino
{
    /// <summary>
    /// This is the user control that is buried in the tabbed, docking panel.
    /// </summary>
    [System.Runtime.InteropServices.Guid("4C10B3EF-7033-4461-9FC3-A0C902D16B88")]
    public partial class V4D4RhinoPanel : UserControl
    {
        /// <summary>
        /// Returns the ID of this panel.
        /// </summary>
        public static System.Guid PanelId => typeof(V4D4RhinoPanel).GUID;

        public static ChromiumWebBrowser Browser;

        private bool closing = false;

        /// <summary>
        /// Public constructor
        /// </summary>
        public V4D4RhinoPanel()
        {
            
            InitializeComponent();
            InitializeCef();
            InitializeChromium();

            V4D4RhinoPlugIn.V4DInterop = new V4DInterop();
            // V4D4RhinoPlugIn.V4DBIMInterop = new V4DBIMInterop();
            V4D4RhinoPlugIn.UIInterop = new UIInterop(Browser, V4D4RhinoPlugIn.V4DInterop);

            Browser.JavascriptObjectRepository.Register("Interop", V4D4RhinoPlugIn.UIInterop, isAsync: true, options: BindingOptions.DefaultBinder);

            Browser.LoadingStateChanged += (sender, args) =>
            {
                //Wait for the Page to finish loading
                if (args.IsLoading == false)
                {
                    // UIInterop.ShowDev();

                    Debug.WriteLine("V4D Content loaded to UI", "V4D");
                    V4D4RhinoPlugIn.UIInterop.SendVersion();

                }
            };

            Browser.AddressChanged += (sender, args) =>
            {
                Debug.WriteLine("new address", "V4D");
            };

            this.Controls.Add(Browser);

            this.Disposed += V4D4RhinoPanel_Disposed;
            // Set the user control property on our plug-in
            V4D4RhinoPlugIn.Instance.PanelUserControl = this;

            RhinoApp.Closing += RhinoApp_Closing;
        }

        public static void InitializeCef()
        {
            if (Cef.IsInitialized) return;

            Cef.EnableHighDPISupport();

            var assemblyLocation = Assembly.GetExecutingAssembly().Location;
            var assemblyPath = Path.GetDirectoryName(assemblyLocation);
            var pathSubprocess = Path.Combine(assemblyPath, "CefSharp.BrowserSubprocess.exe");
            CefSharpSettings.LegacyJavascriptBindingEnabled = true;
            CefSharpSettings.ConcurrentTaskExecution = true;
            var settings = new CefSettings
            {
                LogSeverity = LogSeverity.Verbose,
                LogFile = "ceflog.txt",
                BrowserSubprocessPath = pathSubprocess

            };

            settings.CefCommandLineArgs.Add("allow-file-access-from-files", "1");
            settings.CefCommandLineArgs.Add("disable-web-security", "1");
            Cef.Initialize(settings);
        }

        public static void InitializeChromium()
        {
            if (Browser != null && !Browser.IsDisposed) return;
#if DEBUG
            //use localhost
            Browser = new ChromiumWebBrowser(@"http://localhost:7070/");
            // var index = @"C:\dev\v4design\V4D4Rhino\V4D4RhinoViewer\v4d4rhinoviewer\public\";

#else
            //use app files
            var path = Directory.GetParent(Assembly.GetExecutingAssembly().Location);
            Debug.WriteLine(path, "V4D4Rhino");

            var indexPath = string.Format(@"{0}\app\index.html", path);

            if (!File.Exists(indexPath))
                Debug.WriteLine("V4D4Rhino: Error. The html file doesn't exists : {0}", "V4D4Rhino");

            indexPath = indexPath.Replace("\\", "/");

            Browser = new ChromiumWebBrowser(indexPath);
            //index = indexPath;
#endif
            // Allow the use of local resources in the browser
            Browser.BrowserSettings = new BrowserSettings
            {
                FileAccessFromFileUrls = CefState.Enabled,
                UniversalAccessFromFileUrls = CefState.Enabled
            };

            Browser.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        private void RhinoApp_Closing(object sender, EventArgs e)
        {
            closing = true;
            Dispose(closing);
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (!closing) return;
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            Browser.Dispose();
            Cef.Shutdown();
            base.Dispose(disposing);
        }

        private void OnDispose(object sender, EventArgs e) 
        {
            Debug.WriteLine("Panel OnDisposed", "V4D");
        }

        private void V4D4RhinoPanel_Disposed(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Panel Disposed","V4D");
        }
    }
}
