﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IO.Swagger.BIM.Api;
using IO.Swagger.BIM.Model;

namespace V4D4Rhino
{
    public class V4DBIMInterop
    {
        public IO.Swagger.BIM.Client.Configuration Config { get; set; }

        public V4DBIMInterop()
        {
            Instance = this;
            Config = new IO.Swagger.BIM.Client.Configuration();
            Config.BasePath = "https://kul.jens.app/";
            Debug.WriteLine("V4DIBIMnterop Base Path: " + Config.BasePath);
        }

        public static V4DBIMInterop Instance
        {
            get; private set;
        }

        public async Task<SegmentationResult> StartSegmentationAsync(string id) 
        {
            var instance = new BIMApi(Config);
            SegmentationResult result = await instance.ApiBIMStartSegmentationGetAsync(id);
            return result;
        }

        public async Task<SegmentationResult> GetSegmentationAsync(string id) 
        {
            //var instance = new BIMApi(Config);
            //SegmentationResult result = await instance.ApiBIMGetSegmentationGetAsync(id);
            //return result;

            return await PollSegmentationAsync(id);
        }

        private async Task<SegmentationResult> PollSegmentationAsync(string id) 
        {
            var instance = new BIMApi(Config);
            SegmentationResult result = await instance.ApiBIMGetSegmentationGetAsync(id);
            while (result.Status == "In Queue") 
            {
                await Task.Delay(1000);
                result = await PollSegmentationAsync(id);
            }
            return result;
        }
    }
}
