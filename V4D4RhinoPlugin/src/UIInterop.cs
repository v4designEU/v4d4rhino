﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
#if WIN
using CefSharp;
using CefSharp.WinForms;
#endif

namespace V4D4Rhino
{
    public class UIInterop
    {
#if WIN
        public ChromiumWebBrowser Browser { get; private set; }
#endif
        public V4DInterop V4DInterop { get; private set; }

        // public V4DBIMInterop V4DBIMInterop { get; private set; }

#if WIN
        public UIInterop(ChromiumWebBrowser browser, V4DInterop v4dInterop)
        {
            Browser = browser;
            V4DInterop = v4dInterop;
            // V4DBIMInterop = v4d4BIMInterop;
        }


        public void ShowDev()
        {
            Browser.ShowDevTools();
        }
#endif

        #region To UI (Generic)
        // from SpeckleRhino
        public async Task NotifyFrame(string eventType, string eventInfo)
        {

            var script = string.Format("window.EventBus.$emit('{0}', '{1}')", eventType, eventInfo);
            try
            {
                await Browser.GetMainFrame().EvaluateScriptAsync(script);
            }
            catch
            {
                Debug.WriteLine("For some reason, this browser was not initialised.");
            }
        }
        #endregion

        #region To UI

        public async Task AddEntry(string asset)
        {
            await NotifyFrame("add-asset", asset);
        }

        public void ModifyEntry(dynamic assetData)
        {
            NotifyFrame("modify-asset", JsonConvert.SerializeObject(assetData));
        }

        public async Task SendVersion() 
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            await NotifyFrame("app-version", version);
        } 

        #endregion

        #region From UI

        public void devTools()
        {
            ShowDev();
        }

        /// <summary>
        /// Async bound interop method for user authentication. Called from UI for login or registering user.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="register">If registering, this is true.</param>
        /// <returns>A string with "Success" or "Error" depending on the auth result.</returns>
        public async Task<string> auth(string username, string password, bool register = false)
        {
            return await V4DInterop.AuthAsync(username, password, register);
        }

        /// <summary>
        /// Async bound interop method which gets the latest assets. Called from UI.
        /// </summary>
        /// <param name="args">Dynamic that contains int page, int count, and string assetType</param>
        public async Task getLatest(dynamic args) 
        {
            await V4DInterop.GetLatestAssetsAsync(args.page, args.count, args.assetType);
        }

        /// <summary>
        /// Async bound interop method for requesting a shot reconstruction.
        /// </summary>
        /// <param name="args">Dynamic that contains string id, object shot, and string[] style</param>
        /// <returns></returns>
        public async Task<string> reconstruct(dynamic args)
        {
            return await V4DInterop.ReconstructAsync(args.id, args.shot, args.style);
        }

        public void doQuery (dynamic query)
        {
            
            if (query.text.Contains("http") && query.text.Contains(":\\"))
            {
                // do crawling
                var response = V4DInterop.Crawl(query);

                NotifyFrame("init-crawl", response);

            } else {

                // query
                var tags = new List<string>();
                if (query.tags != null)
                {
                    foreach (var tag in query.tags)
                        tags.Add(tag);
                    // TODO: V2 -- Reimplement Search By Tags
/*
                    var assets = V4DInterop.SearchByTags(0, 100, tags);
                    NotifyFrame("clear-assets", "");
                    foreach (var asset in assets)
                    {
                        AddEntry(asset.ToJSON());
                    }
                    */
                } else {
                    NotifyFrame("clear-assets", "");
                    //getLatestAssets();
                }


                NotifyFrame("STOP_LOADER", "");

            }
        }

        public async Task<bool> search(dynamic args) 
        {
            var success = await V4DInterop.SearchAsync(args.page, args.count, args.searchTerm, args.filterBy, args.name, args.description, args.location, args.style, args._abstract, args.comment, args.building);
            return success;
        }

        public async Task style(dynamic args) 
        {
            await V4DInterop.StyleAsync(args.assetId, args.modelId, args.style);
        }

        public async Task rate(dynamic args) 
        {
            await V4DInterop.RateAsync(args.assetId, args.rating, args.comment);
        }

        #region ImportAsset
        public void selectFile() 
        {
            Action openFile = new Action(async () => 
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                //openFileDialog1.InitialDirectory = "c:\\";
                openFileDialog1.Filter = "Video files (*.mp4)|*.mp4;";
                openFileDialog1.FilterIndex = 0;
                openFileDialog1.RestoreDirectory = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string selectedFileName = openFileDialog1.FileName;
                    //...
                    var data = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(selectedFileName));
                    await NotifyFrame("file-selected", data);
                }
            });

            Rhino.RhinoApp.InvokeOnUiThread(openFile);

        }

        public string importAsset(dynamic args)
        {

            string tags = args.tags as string;
            tags = tags.Replace(" ", string.Empty);
            var tagList = tags.Split(',').ToList();
            tagList.ForEach(t => t = "\"" + t + "\"");

            string response = V4DInterop.ImportAsset(args.file, args.name, args.description, tagList);
            return response;
        }

        #endregion

        public async void downloadAsset(dynamic args)
        {
            await V4DInterop.DownloadAsset(args.id, args.url);
        }

        public async Task downloadAssets(dynamic args) 
        {
            var urls = new List<string>();
            foreach (var url in args.urls)
                urls.Add(url);
            await V4DInterop.DownloadAssetsAsync(args.id, urls);
        }

        public async Task addBookmarkedAsset(dynamic data) 
        {
            await V4DInterop.AddBookmark(data.assetId);
        }

        #region BIM

        /*
        public async Task startSegmentation(string id) 
        {
            var results = await V4DBIMInterop.StartSegmentationAsync(id);
        }

        public async Task getSegmentation(string id)
        {
            var results = await V4DBIMInterop.GetSegmentationAsync(id);
        }
        */

        #endregion

        public void addModel(string assetId)
        {
            var appDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "V4Design");
            var tmp = Path.Combine(appDir, assetId);

            var path = tmp;//Path.Combine(@"C:\data", assetId);
            var zip = Path.ChangeExtension(path, ".zip");
            

            var files = unzipAsset(zip, assetId);
            importFiles(files, assetId);

            var zipBIM = path + "_BIM.zip";

            if (File.Exists(zipBIM))
            {
                var bimFiles = unzipAsset(zipBIM, assetId);
                importFiles(bimFiles, assetId);
            }
        }

        public Task setTexture(dynamic textureData)
        {

            string assetId = textureData.id as string;
            int? textureId = textureData.textureId as int?;
            // get the asset 
            var asset = V4DInterop.Assets.Find(a => a.AssetId == assetId);
            // check if it has rhino ojects

            // TODO: Fix RhinoObject registration for V2
            if(asset.RhinoObjectIds != null && asset.RhinoObjectIds.Count > 0)
            {
                // check if those rhino objects exist in the model
                foreach(var id in asset.RhinoObjectIds)
                {
                    var obj = Rhino.RhinoDoc.ActiveDoc.Objects.FindId(id);
                    if(obj != null)
                    {
                        // update the texture
                        var mat = Rhino.RhinoDoc.ActiveDoc.Materials[obj.Attributes.MaterialIndex];
                        
                        var tex = mat.GetTextures().ToList();
                        var bm = tex.Find(t => t.TextureType == Rhino.DocObjects.TextureType.Bitmap);

                        var appDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "V4Design");
                        var assetDir = Path.Combine(appDir, asset.AssetId);
                        
                        var texureDir = Path.Combine(assetDir, textureId.ToString());

                        if (textureId == 0)
                            texureDir = assetDir;

                        var filename = Path.GetFileName(bm.FileName);
                        //bm.FileName = Path.Combine(texureDir, filename);

                        mat.SetBitmapTexture(Path.Combine(texureDir, filename));

                        mat.CommitChanges();

                    }
                }

            }

            return null;

        }

        public async Task createMaterial(string imageUrl)
        {
            var doc = Rhino.RhinoDoc.ActiveDoc;
            var mat = new Rhino.DocObjects.Material();

            var imageName = Path.GetFileName(imageUrl);
            var appDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "V4Design");
            var tmp = Path.Combine(appDir, imageName);

            using (WebClient client = new WebClient())
            {
                client.DownloadFileAsync(new Uri(imageUrl), tmp);
            }

            mat.SetBitmapTexture(tmp);

            var rmat = Rhino.Render.RenderMaterial.CreateBasicMaterial(mat);
            doc.RenderMaterials.Add(rmat);
            return;
        }

        #endregion

        #region utility

        public string [] unzipAsset(string filename, string assetId)
        {
            var path = Path.GetDirectoryName(filename);
            var name = Path.GetFileNameWithoutExtension(filename);
            if (filename.Contains("BIM"))
                name = name.Replace("_BIM", string.Empty);
            var dir = Path.Combine(path, name);

            if (Directory.Exists(dir) && filename.Contains("BIM"))
            {
                dir = Path.Combine(dir, "BIM");

            }

            Directory.CreateDirectory(dir);
            
            try
                { 
                ZipFile.ExtractToDirectory(filename, dir);
                var subDirs = Directory.GetDirectories(dir);

                var asset = V4DInterop.Assets.Find(a => a.AssetId == assetId);
                /* TODO: fix optional textures for V2
                // asset.HasOptionalTextures = subDirs.Length > 0 ? true : false;

                if (asset.HasOptionalTextures)
                {
                    dynamic assetData = new System.Dynamic.ExpandoObject();
                    assetData.Id = assetId;
                    assetData.Property = "HasOptionalTextures";
                    assetData.Value = "true";
                    ModifyEntry(assetData);
                }
                */

            }
            catch (Exception ex)
            {
                //more than likely hit due to files already existing
                Debug.WriteLine(ex.Message);
            }

            return Directory.GetFiles(dir,"*.*",SearchOption.TopDirectoryOnly);
        }

        public void importFiles(string [] files, string assetId)
        {
            var asset = V4DInterop.Assets.Find(a => a.AssetId == assetId);

            var layerName = "V4D_" + asset.AssetId;

            var parentLayer = Rhino.RhinoDoc.ActiveDoc.Layers.FindName(layerName);

            if (parentLayer == null)
                Rhino.RhinoDoc.ActiveDoc.Layers.Add(layerName, System.Drawing.Color.Black);

            parentLayer = Rhino.RhinoDoc.ActiveDoc.Layers.FindName(layerName);
            var parentLayerId = parentLayer.Id;

            Rhino.RhinoDoc.ActiveDoc.Layers.SetCurrentLayerIndex(parentLayer.Index, true);

            var layer = new Rhino.DocObjects.Layer();
            layer.Name = "Textured Asset";
            layer.ParentLayerId = parentLayerId;
            layer.SetUserString("assetId", assetId);

            if (Rhino.RhinoDoc.ActiveDoc.Layers.FindByFullPath(layerName + "::" + layer.Name, Rhino.RhinoMath.UnsetIntIndex) > 0) 
            {
                layer.Name = "BIM Asset";
            }

            var layerIndex = Rhino.RhinoDoc.ActiveDoc.Layers.Add(layer);

            var layerId = Rhino.RhinoDoc.ActiveDoc.Layers.FindIndex(layerIndex).Id;

            Rhino.RhinoDoc.ActiveDoc.Views.RedrawEnabled = false;

            var modelFiles = files.Where(f => f.Contains(".obj")).ToList();

            var index = modelFiles.FindIndex(f => f.Contains("SE_Mesh.obj"));

            if (index != -1)
                modelFiles.RemoveAt(index);

            foreach (var file in modelFiles)
            {
                //var noExt = Path.GetFileNameWithoutExtension(file);
                //var lastSlash = file.LastIndexOf('\\');
                //var assetId = file.Substring(0, lastSlash);
                //lastSlash = assetId.LastIndexOf('\\')+1;
                //assetId = assetId.Substring(lastSlash, assetId.Length- lastSlash);

                var fro = new Rhino.FileIO.FileReadOptions()
                {
                    ImportMode = true,
                    ScaleGeometry= false,
                    UseScaleGeometry = true,
                };
                var foro = new Rhino.FileIO.FileObjReadOptions (fro)
                {
                    MapYtoZ = true
                };

                Rhino.FileIO.FileObj.Read(file, Rhino.RhinoDoc.ActiveDoc, foro);

                /* var objs = Rhino.RhinoDoc.ActiveDoc.Objects.GetObjectList(new Rhino.DocObjects.ObjectEnumeratorSettings 
                {
                    LayerIndexFilter = layerId
                });
                */

                var objs = Rhino.RhinoDoc.ActiveDoc.Objects.FindByLayer(layerName);

                //var objs = Rhino.RhinoDoc.ActiveDoc.Objects.GetSelectedObjects(false, false);

                if (asset.RhinoObjectIds == null )
                    asset.RhinoObjectIds = new List<Guid>();

                foreach (var o in objs) 
                {
                    o.Attributes.LayerIndex = layerIndex;
                    if(layer.Name == "Textured Asset")
                        asset.RhinoObjectIds.Add(o.Id);

                    if (layer.Name == "BIM Asset") 
                    {
                        // get / create layer
                        var matId = o.Attributes.MaterialIndex;
                        var matName = Rhino.RhinoDoc.ActiveDoc.Materials.FindIndex(matId).Name;
                        var c_layer = new Rhino.DocObjects.Layer();
                        c_layer.ParentLayerId = layerId;

                        if (matName.Contains("Clutter")) 
                        {
                            c_layer.Name = "Clutter";
                            c_layer.Color = System.Drawing.Color.FromArgb(255, 255, 255);
                        }

                        if (matName.Contains("Beams"))
                        {
                            c_layer.Name = "Beams";
                            c_layer.Color = System.Drawing.Color.FromArgb(255, 255, 0);
                        }

                        if (matName.Contains("Walls"))
                        {
                            c_layer.Name = "Walls";
                            c_layer.Color = System.Drawing.Color.FromArgb(0, 128, 0);
                        }

                        if (matName.Contains("Roofs"))
                        {
                            c_layer.Name = "Roofs";
                            c_layer.Color = System.Drawing.Color.FromArgb(0, 0, 255);
                        }

                        if (matName.Contains("Ceilings"))
                        {
                            c_layer.Name = "Ceilings";
                            c_layer.Color = System.Drawing.Color.FromArgb(128, 0, 128);
                        }

                        var c_layer_index = Rhino.RhinoDoc.ActiveDoc.Layers.FindByFullPath(parentLayer.Name + "::" + layer.Name + "::" + c_layer.Name, Rhino.RhinoMath.UnsetIntIndex);
                        if (c_layer_index == Rhino.RhinoMath.UnsetIntIndex)
                            c_layer_index = Rhino.RhinoDoc.ActiveDoc.Layers.Add(c_layer);
                        o.Attributes.LayerIndex = c_layer_index;
                    }
                    o.CommitChanges();
                }

                /* TODO: Fix rhinoObject registration for V2
                asset.RhinoObjectIds = new List<Guid>();
                foreach (var o in objs)
                {
                    Debug.WriteLine(o.Id);
                    asset.RhinoObjectIds.Add(o.Id);
                }

                dynamic assetData = new System.Dynamic.ExpandoObject();
                assetData.Id = assetId;
                assetData.Property = "RhinoObjectIds";
                assetData.Value = asset.RhinoObjectIds;
                ModifyEntry(assetData);
                */
                NotifyFrame("model-added", assetId);

            }

            Rhino.RhinoDoc.ActiveDoc.Views.RedrawEnabled = true;
            Rhino.RhinoDoc.ActiveDoc.Views.Redraw();

            // Action redraw = Rhino.RhinoDoc.ActiveDoc.Views.Redraw;
            // Rhino.RhinoApp.InvokeAndWait(redraw);

        }

        public void doRunScript(string script)
        {
            Rhino.RhinoApp.RunScript(script, false);
        }

        private AsyncCompletedEventHandler Client_DownloadFileCompleted(string filename)
        {
            Action<object, AsyncCompletedEventArgs> action = (sender, e) => 
            {
                var _filename = filename;

                try 
                {
                    long length = new System.IO.FileInfo(filename).Length;
                    if (length > 0)
                    {
                        Debug.WriteLine("Download File Completed", "V4D4Rhino");

                        var name = Path.GetFileNameWithoutExtension(_filename);
                        NotifyFrame("download-complete", name);
                    }
                    else {

                        throw new FileLoadException("Empty File");
                    }

                } catch (Exception ex) 
                {
                    Debug.WriteLine("Download File Failed", "V4D4Rhino");
                }
            };
            return new AsyncCompletedEventHandler(action);
        }


        private DownloadProgressChangedEventHandler Client_DownloadProgressChanged(string assetId)
        {
            var progress = 0;
            Action<object, DownloadProgressChangedEventArgs> action = (sender, e) =>
            {
                var _assetId = assetId;

                var dict = new Dictionary<string, object>();
                dict.Add("id", _assetId);
                dict.Add("value", e.ProgressPercentage);

                // do stuff...

                if ((e.ProgressPercentage - progress) >= 5)
                {
                    //Debug.WriteLine("Download Progress: " + e.ProgressPercentage);
                    NotifyFrame("download-progress-update", JsonConvert.SerializeObject(dict));
                    progress = e.ProgressPercentage;
                }

            };
            return new DownloadProgressChangedEventHandler(action);

        }

        #endregion



    }
}
