﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using CefSharp;
using CefSharp.WinForms;
using Rhino.PlugIns;
using Rhino.UI;


namespace V4D4Rhino
{
    ///<summary>
    /// <para>Every RhinoCommon .rhp assembly must have one and only one PlugIn-derived
    /// class. DO NOT create instances of this class yourself. It is the
    /// responsibility of Rhino to create an instance of this class.</para>
    /// <para>To complete plug-in information, please also see all PlugInDescription
    /// attributes in AssemblyInfo.cs (you might need to click "Project" ->
    /// "Show All Files" to see it in the "Solution Explorer" window).</para>
    ///</summary>
    public class V4D4RhinoPlugIn : Rhino.PlugIns.PlugIn
    {

        public static UIInterop UIInterop;

        public static V4DInterop V4DInterop;

        // public static V4DBIMInterop V4DBIMInterop;

        //string index = null;

        public V4D4RhinoPlugIn()
        {
            Instance = this;
        }

        ///<summary>Gets the only instance of the V4D4RhinoPlugIn plug-in.</summary>
        public static V4D4RhinoPlugIn Instance
        {
            get; private set;
        }

        /// <summary>
        /// The tabbed dockbar user control
        /// </summary>
        public V4D4RhinoPanel PanelUserControl { get; set; }

        // public V4DRhinoPanelWebView PanelUserControlWV { get; set; }

        // You can override methods here to change the plug-in behavior on
        // loading and shut down, add options pages to the Rhino _Option command
        // and maintain plug-in wide options in a document.

        protected override LoadReturnCode OnLoad(ref string errorMessage)
        {

            Panels.RegisterPanel(this, typeof(V4D4RhinoPanel), "V4D4Rhino", V4D4Rhino.Properties.Resources.v4d_logo, PanelType.System);

            var tmp = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "V4Design");

            if (!Directory.Exists(tmp))
                Directory.CreateDirectory(tmp);

            return base.OnLoad(ref errorMessage);
        }

        protected override void OnShutdown()
        {
            V4D4RhinoPlugIn.Instance.PanelUserControl?.Dispose();
            base.OnShutdown();

        }
    }
}