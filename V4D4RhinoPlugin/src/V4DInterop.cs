﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Linq;
using IO.Swagger.Api;
using IO.Swagger.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace V4D4Rhino
{
    public class V4DInterop
    {

        public IO.Swagger.Client.Configuration Config { get; set; }

        public List<AssetDetails> Assets { get; set; }

        public List<AssetDetails> BookmarkedAssets { get; set; }

        public string IndexPath { get; set; }

        public V4DInterop()
        { 
            Instance = this;
        }

        ///<summary>Gets the only instance of the V4D4RhinoPlugIn plug-in.</summary>
        public static V4DInterop Instance
        {
            get; private set;
        }
        #region User Authentication
        /// <summary>
        /// Authorize user (async). This method handles user login or registration.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="register">If registering, this is true.</param>
        /// <returns>A string with "Success" or "Error" depending on the auth result.</returns>
        public async Task<string> AuthAsync(string username, string password, bool register)
        {
            Config = new IO.Swagger.Client.Configuration
            {
                Username = username,
                Password = password,
                Timeout = 300000
            };

            Debug.WriteLine("V4DInterop Base Path: " + Config.BasePath);

            var instance = new UserApi(Config);

            InlineResponse200 response;
            try
            {
                if (register)
                {
                    response = await instance.UserCreatePostAsync(new UserDetails1(Config.Username, Config.Password));
                }
                else
                {
                    response = await instance.UserLoginPostAsync(new UserDetails2(Config.Username, Config.Password));
                }

                if (response.Result != "Error")
                {
                    Config.AddDefaultHeader("X-V4DesignSession", response.SessionId);
                }

                return response.Result;
            }
            catch (IO.Swagger.Client.ApiException ex) 
            {
                Debug.WriteLine(ex.Message);

                return "Error";
            }

        }

        #endregion

        #region Get Latest Assets
        /// <summary>
        /// Gets the latest assets from V4D repository.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="count"></param>
        /// <param name="assetType"></param>
        /// <returns>A task that completes when all assets requested have been processed.</returns>
        public async Task GetLatestAssetsAsync(long? page = 0, long? count = 0, string assetType = null) 
        {
            Assets = new List<AssetDetails>();

            List<Task> listToProcess = new List<Task>();

            var assetInstance = new AssetsApi(Config);

            var searchParams = new SearchParams(page, count);
            var response = await assetInstance.AssetsLatestPostAsync(searchParams, assetType);

            var toProcess = response.Assets.Select(async a => 
            {
                await ProcessAssetAsync(a);
            }).ToList();
            /*
            foreach (var asset in response.Assets)
            {
                listToProcess.Add(ProcessAssetAsync(asset));
            }
            */

            await Task.WhenAll(toProcess);
        }


        public async Task<bool> SearchAsync(long? page = 0, long? count = 0, string searchTerm = "", List<SearchParams1.FilterByEnum> filterBy = null,
            string name = "", string description = "", string location = "",
            string style = "", string _abstract = "", string comment = "",
            string building = "")
        {
            var assetInstance = new AssetsApi(Config);
            var searchParams = new SearchParams1(page, count, searchTerm, filterBy, name, description, location, style, _abstract, comment, building);
            var response = await assetInstance.AssetsSearchPostAsync(searchParams);
            
            Assets = new List<AssetDetails>();

            if (response.Assets == null || response.Assets.Count == 0) return false;

            var toProcess = response.Assets.Select(async a =>
            {
                await ProcessAssetAsync(a);
            }).ToList();

            await Task.WhenAll(toProcess);
            return true;
        }

        async Task ProcessAssetAsync(AssetDetails asset)
        {
            
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new DataContractResolver(),// new CamelCasePropertyNamesContractResolver(),
                Formatting = Formatting.None,
                StringEscapeHandling = StringEscapeHandling.EscapeHtml,
                NullValueHandling = NullValueHandling.Include
            };

            // filter based on potential errors
            // 1. ThumbnailUrl contains .bin
            // 2. Filled but not existing fields in model url and thumbnail url
            // 3. Existing file in model url, but non - existing in thumbnail url
 
            // #1
            if ( !string.IsNullOrEmpty(asset.ThumbnailUrl) && asset.ThumbnailUrl.Contains(".bin")) return;

            var dlUrl = asset.DownloadUrl;
            var tUrl = asset.ThumbnailUrl;

            if (string.IsNullOrEmpty(tUrl))
            {
                if (asset.Shots != null && asset.Shots.Count > 0)
                    if (asset.Shots.Count > 1)
                        asset.ThumbnailUrl = asset.Shots[1].PreviewUrl;
                    else
                        asset.ThumbnailUrl = asset.Shots[0].PreviewUrl;
            }

            if (!string.IsNullOrEmpty(dlUrl as string)) 
            {
                //Debug.WriteLine(asset.AssetId, "V4D");
                //Debug.WriteLine("Basic Asset", "V4D");

                if (string.IsNullOrEmpty(tUrl)) 
                {
                    if (asset.Shots != null && asset.Shots.Count > 0)
                        asset.ThumbnailUrl = asset.Shots[0].PreviewUrl;
                }

            }

            if (string.IsNullOrEmpty(asset.ThumbnailUrl) && string.IsNullOrEmpty(dlUrl as string)) return;

            asset.Name = string.IsNullOrEmpty(asset.Name) ? "Undefined" : asset.Name;

            // check if asset has already been downloaded

            var appDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "V4Design");
            var assetDir = Path.Combine(appDir, asset.AssetId);
            // var downloadURL = Directory.Exists(assetDir) ? assetDir : asset.DownloadUrl;
            if (dlUrl != null && dlUrl.GetType() != typeof(byte[]))
            {
                //check if it has been downloaded before
                // Debug.WriteLine("dl url not null");
                asset.DownloadUrl = Directory.Exists(assetDir) ? System.Text.Encoding.UTF8.GetBytes(assetDir) : System.Text.Encoding.UTF8.GetBytes(asset.DownloadUrl.ToString());

            }
                
            if (asset.Description != null) 
            {
                //var d_dict =  JsonConvert.DeserializeObject(asset.Description);

                //asset.Description = JsonConvert.DeserializeObject<Dictionary<string, string>>(asset.Description.ToString());

                //var d_dict = new Dictionary<string, string>();

                var list = new List<object>();

                asset.Description.ForEach(d => {

                    var d_dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(d.ToString());

                    foreach (var kvp in d_dict)
                        list.Add(kvp);

                });

                asset.Description = list;

            }

            if (asset.BIM != null) 
            {
                var list = new List<object>();
                asset.BIM.ForEach(b => 
                {
                    var b_dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(b.ToString());
                    foreach (var kvp in b_dict)
                        list.Add(kvp);
                });

                asset.BIM = list;

            }

            Assets.Add(asset);

            var s_asset = JsonConvert.SerializeObject(asset, settings);

            V4D4RhinoPlugIn.UIInterop.AddEntry(s_asset);

        }

        #endregion

        public async Task AddBookmark(string assetId) 
        {
            if (BookmarkedAssets == null)
                BookmarkedAssets = new List<AssetDetails>();

            // asset = JsonConvert.DeserializeObject<AssetDetails>(data);
            if (BookmarkedAssets.Any(a => a.AssetId == assetId)) return;
            
            BookmarkedAssets.Add( Assets.First(a => a.AssetId == assetId));
        }

        public async Task<string> ReconstructAsync(string id, dynamic shot, dynamic styles) 
        {
            var asset = Assets.Find(a => a.AssetId == id);

            var simmo = asset.Simmo[0].ToString();

            var styleList = new List<string>();
            if(styles != null)
                foreach (var s in styles)
                    styleList.Add(s);

            var assetInstance = new AssetsApi(Config);
            var recReq = new ReconstructRequest(
                simmo, 
                long.Parse(shot.shot.firstFrame),
                long.Parse(shot.shot.firstFrame),
                long.Parse(shot.shot.lastFrame),
                shot.shot.previewUrl,
                "",
                shot.id,
                styleList
                );
            

            var response = await assetInstance.AssetsReconstructPostAsync(recReq);
            return response.Result;
        }

        public async Task Search(int page, int count, string query) 
        {
            List<Task> listToProcess = new List<Task>();

            var assetInstance = new AssetsApi(Config);

            var searchParams = new SearchParams1(page, count, query);
            var response = assetInstance.AssetsSearchPost(searchParams);

            if (response.Assets == null) return;

            Assets = response.Assets;

            foreach (var asset in response.Assets)
            {
                listToProcess.Add(ProcessAssetAsync(asset));
            }

            await Task.WhenAll(listToProcess);
        }


        public async Task StyleAsync(string assetId, string modelId, string style) 
        {
            var assetInstance = new AssetsApi(Config);
            var styleRequest = new StyleRequest(modelId, style);
            var response = await assetInstance.AssetsStyleAssetIdPostAsync(assetId, styleRequest);
        }

        public async Task<string> RateAsync(string assetId, int rating, string comment) 
        {
            var ratingInstance = new RatingApi(Config);
            var assetRating = new AssetRating(rating, comment);
            var response = await ratingInstance.RatingRateAssetIdPostAsync(assetId, assetRating);
            return response.Result;
        }

        public string ImportAsset(string assetFilePath, string assetName, string assetDescription, List<string> assetTags) 
        {
            Stream fs = File.OpenRead(assetFilePath);
            var assetInstance = new AssetsApi(Config);

            try
            {
                var response = assetInstance.AssetsImportPost(fs, assetName, assetDescription, assetTags);
                return response.Result;
            }
            catch (Exception ex) 
            {
                Debug.WriteLine(ex.Message);
                return "Error"; 
            }
        }

        public async Task DownloadAssetsAsync(string assetId, List<string> urls) 
        {
            dlNeed = urls.Count;
            foreach (var url in urls) 
            {
                await DownloadAsset(assetId, url);
            }
        }

        private int dlCount = 0;
        private int dlNeed = 1;

        public async Task DownloadAsset(string assetId, string url) 
        {
            var appDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "V4Design");
            var fileId = assetId;
            if (url.Contains("BIM"))
                fileId += "_BIM";
            var tmp = Path.Combine(appDir, fileId + ".zip");
            string localAssetFilename = tmp;

            var uri = new Uri(url);

            using (var client = new WebClient())
            {
                var sessionId = Config.DefaultHeader["X-V4DesignSession"];
                
                if(!url.Contains("BIM"))
                    client.Headers.Add("X-V4DesignSession", sessionId);
                client.DownloadProgressChanged += Client_DownloadProgressChanged(assetId);
                client.DownloadFileCompleted += Client_DownloadFileCompleted(localAssetFilename);
                client.DownloadFileAsync(uri, localAssetFilename);
            }
        }

        private AsyncCompletedEventHandler Client_DownloadFileCompleted(string filename)
        {
            void action(object sender, AsyncCompletedEventArgs e)
            {
                var _filename = filename;

                try
                {
                    long length = new System.IO.FileInfo(filename).Length;
                    if (length > 0)
                    {
                        Debug.WriteLine("Download File Completed", "V4D4Rhino");
                        var name = Path.GetFileNameWithoutExtension(_filename);
                        dlCount++;
                        if (dlCount == dlNeed)
                        {
                            if (name.Contains("_BIM")) 
                                name = name.Replace("_BIM", string.Empty);

                            V4D4RhinoPlugIn.UIInterop.NotifyFrame("download-complete", name);
                            dlCount = 0;
                            dlNeed = 1;
                        }
                    }
                    else
                    {

                        throw new FileLoadException("Empty File");
                    }

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    Debug.WriteLine("Download File Failed", "V4D4Rhino");
                }
            }
            return new AsyncCompletedEventHandler(action);
        }

        private DownloadProgressChangedEventHandler Client_DownloadProgressChanged(string assetId)
        {
            var progress = 0;
            void action(object sender, DownloadProgressChangedEventArgs e)
            {
                var _assetId = assetId;

                var dict = new Dictionary<string, object>();
                dict.Add("id", _assetId);
                dict.Add("value", e.ProgressPercentage);

                // do stuff...

                if ((e.ProgressPercentage - progress) >= 5)
                {
                    //Debug.WriteLine("Download Progress: " + e.ProgressPercentage);
                    V4D4RhinoPlugIn.UIInterop.NotifyFrame("download-progress-update", JsonConvert.SerializeObject(dict));
                    progress = e.ProgressPercentage;
                }

            }
            return new DownloadProgressChangedEventHandler(action);

        }

        // TODO: V2 -- Reimplement Search By Tags

        /*
        public List<V4DAsset> SearchByTags(int page, int count, List<string> tags)
        {
            var assetInstance = new AssetsApi(Config);
           // var searchParams = new SearchParams1(page, count, tags);
           // var response = assetInstance.AssetsSearchByTagsPost(searchParams);

            var assets = new List<V4DAsset>();

            foreach (var asset in response.Assets)
            {
                if (asset.DownloadUrl != null)
                {
                    var appDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "V4Design");
                    var assetDir = Path.Combine(appDir, asset.AssetId);
                    var downloadURL = Directory.Exists(assetDir) ? assetDir : asset.DownloadUrl;

                    var v4dAsset = new V4DAsset
                    {
                        ThumbnailURL = asset.ThumbnailUrl != null ? asset.ThumbnailUrl : "",
                        ModelURL = System.Text.Encoding.UTF8.GetBytes(downloadURL),
                        Name = string.IsNullOrEmpty(asset.Name) ? System.Text.Encoding.UTF8.GetBytes("Undefined") : System.Text.Encoding.UTF8.GetBytes(asset.Name),
                        VertexCount = asset.Polygons != null ? (int)asset.Polygons : 0,
                        FaceCount = asset.FaceCount != null ? (int)asset.FaceCount : 0,
                        Style = System.Text.Encoding.UTF8.GetBytes(asset.Style),
                        Building = System.Text.Encoding.UTF8.GetBytes(asset.Building),
                        Creator = System.Text.Encoding.UTF8.GetBytes(asset.Creator),
                        Nickname = System.Text.Encoding.UTF8.GetBytes(asset.Nickname),
                        Organization = System.Text.Encoding.UTF8.GetBytes(asset.Organisation),
                        Person = System.Text.Encoding.UTF8.GetBytes(asset.Person),
                        Description = System.Text.Encoding.UTF8.GetBytes(asset.Description),
                        Location = System.Text.Encoding.UTF8.GetBytes(asset.Location),
                        Source = asset.AssetId,
                        Year = asset.EraYear.ToString(),
                        Tags = asset.Tags,
                        Id = asset.AssetId,
                        KbId = asset.KBID
                    };

                    assets.Add(v4dAsset);
                }
            }

            return assets;
        }

    */
        

        public string Crawl(string url)
        {
            var instance = new OtherApi(Config);
            var urlToCrawl = new Url(url);
            var response = instance.CrawlerCrawlUrlPost(urlToCrawl);
            return response.Result;
        }

        public string GetFileSize(string url)
        {
            long result = -1;
            string res;

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "HEAD";
            request.Timeout = 5000;

            //string result;

            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                if (long.TryParse(response.Headers.Get("Content-Length"), out long ContentLength))
                {
                    result = ContentLength;
                }

                response.Dispose();

                res = result.ToString();

            }
            catch (Exception ex)
            {
                res = ex.Message;
            }



            return res;

        }

        /// <summary>
        /// https://stackoverflow.com/questions/275689/how-to-get-relative-path-from-absolute-path
        /// Creates a relative path from one file or folder to another.
        /// </summary>
        /// <param name="fromPath">Contains the directory that defines the start of the relative path.</param>
        /// <param name="toPath">Contains the path that defines the endpoint of the relative path.</param>
        /// <returns>The relative path from the start directory to the end path or <c>toPath</c> if the paths are not related.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="UriFormatException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public static String MakeRelativePath(String fromPath, String toPath)
        {
            if (String.IsNullOrEmpty(fromPath)) throw new ArgumentNullException("fromPath");
            if (String.IsNullOrEmpty(toPath)) throw new ArgumentNullException("toPath");

            Uri fromUri = new Uri(fromPath);
            Uri toUri = new Uri(toPath);

            if (fromUri.Scheme != toUri.Scheme) { return toPath; } // path can't be made relative.

            Uri relativeUri = fromUri.MakeRelativeUri(toUri);
            String relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            if (toUri.Scheme.Equals("file", StringComparison.InvariantCultureIgnoreCase))
            {
                relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            }

            return relativePath;
        }

        public async Task<HttpStatusCode> GetUrlStatus(string url, string userAgent)
        {
            HttpStatusCode result = default(HttpStatusCode);

            userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = userAgent;

            var sessionId = Config.DefaultHeader["X-V4DesignSession"];
            request.Headers.Add("X-V4DesignSession", sessionId);

            request.Method = "HEAD";
            request.Timeout = 1000;

            try
            {
                using (var response = await request.GetResponseAsync() as HttpWebResponse)
                {
                    if (response != null)
                    {
                        result = response.StatusCode;
                        response.Close();
                    }
                }
            }
            catch (WebException we)
            {

                // Debug.WriteLine(we.Message);
                
                var res = (HttpWebResponse)we.Response;

                if (res == null)
                {
                    result = HttpStatusCode.RequestTimeout;
                    Debug.WriteLine("timed out");
                } else {
                    result = res.StatusCode;
                }
                
            }

            return result;
        }

    }

    public class DataContractResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);
            property.NullValueHandling = NullValueHandling.Include;
            property.DefaultValueHandling = DefaultValueHandling.Include;
            property.ShouldSerialize = o => true;
            return property;
        }
    }


}
