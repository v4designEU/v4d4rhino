function proccesAsset (asset) {
  let _asset = {}

  // assetId
  _asset.assetId = asset.hasOwnProperty('assetId') ? asset.assetId : null

  // name
  if (asset.hasOwnProperty('name') && asset.name.length > 0) {
    _asset.name = asset.name
  } else {
    _asset.name = 'Undefined'
  }

  // downloadUrl
  _asset.downloadUrl = asset.hasOwnProperty('downloadUrl') ? asset.downloadUrl : null

  // thumbnailUrl
  _asset.thumbnailUrl = asset.hasOwnProperty('thumbnailUrl') ? asset.thumbnailUrl : null
  // console.log(asset.hasOwnProperty('thumbnailUrl'))
  // console.log(_asset.thumbnailUrl)

  // 3DModelThumbnailUrl
  _asset['3DModelThumbnailUrl'] = asset.hasOwnProperty('3DModelThumbnailUrl') ? asset['3DModelThumbnailUrl'] : null

  // Description
  if (asset.hasOwnProperty('description') && asset.description.length > 0) {
    _asset.description = asset.description
  } else {
    _asset.description = null
  }

  // Latitude
  _asset.latitude = asset.hasOwnProperty('latitude') ? asset.latitude : null

  // Longitude
  _asset.longitude = asset.hasOwnProperty('longitude') ? asset.longitude : null

  // Tags
  if (asset.hasOwnProperty('tags') && asset.tags.length > 0) {
    _asset.tags = asset.tags
  } else {
    _asset.tags = null
  }

  // Location
  _asset.location = asset.hasOwnProperty('location') ? asset.location : null

  // License
  if (asset.hasOwnProperty('license') && asset.license.length > 0) {
    _asset.license = asset.license
  } else {
    _asset.license = null
  }

  // Source
  if (asset.hasOwnProperty('source') && asset.source.length > 0) {
    _asset.source = asset.source
  } else {
    _asset.source = null
  }

  // PublicationData
  if (asset.hasOwnProperty('publicationDate') && asset.publicationDate.length > 0) {
    _asset.publicationDate = asset.publicationDate
  } else {
    _asset.publicationDate = null
  }

  // ReconstructionDate
  _asset.reconstructionDate = asset.hasOwnProperty('reconstructionDate') ? asset.reconstructionDate : null

  // BasicAssets
  if (asset.hasOwnProperty('basicAssets') && asset.basicAssets.length > 0) {
    _asset.basicAssets = asset.basicAssets
  } else {
    _asset.basicAssets = null
  }

  // ShotIds
  if (asset.hasOwnProperty('shotIds') && asset.shotIds.length > 0) {
    _asset.shotIds = asset.shotIds
  } else {
    _asset.shotIds = null
  }

  // Textures
  if (asset.hasOwnProperty('textures') && asset.textures.length > 0) {
    _asset.textures = asset.textures
  } else {
    _asset.textures = null
  }

  // Shots
  if (asset.hasOwnProperty('shots') && asset.shots.length > 0) {
    _asset.shots = asset.shots
  } else {
    _asset.shots = null
  }

  // check thumbnailUrl
  if (_asset.thumbnailUrl === null) {
    if (_asset.shots !== null) {
      _asset.thumbnailUrl = _asset.shots[0].previewUrl
    } else {
      _asset.thumbnailUrl = 'https://cdn4.iconfinder.com/data/icons/internet-and-web/80/Internet_icons-09-512.png'
    }
  }

  // KBID
  _asset.kbid = asset.hasOwnProperty('kbid') ? asset.kbid : null

  // LatestRatings
  if (asset.hasOwnProperty('latestRatings') && JSON.stringify(asset.latestRatings) !== '{}') {
    _asset.latestRatings = asset.latestRatings
  } else {
    _asset.latestRatings = null
  }

  // Errors
  if (asset.hasOwnProperty('errors') && asset.errors.length > 0) {
    _asset.errors = asset.errors
  } else {
    _asset.errors = null
  }

  // Simmo
  if (asset.hasOwnProperty('simmo') && asset.simmo.length > 0) {
    _asset.simmo = asset.simmo
  } else {
    _asset.simmo = null
  }

  // console.log(asset)
  // console.log(_asset)
  // console.log('--------------------------------------')
  return _asset
}

function matchQuery (assets, text) {
  return assets.filter(item =>
    item.assetId.toLowerCase().includes(text.toLowerCase()) ||
    item.name.toLowerCase().includes(text.toLowerCase()) ||
    item.description.map(a => `(${Object.values(a)})`).join('').toLowerCase().includes(text.toLowerCase()) ||
    item.tags.join(' ').toLowerCase().includes(text.toLowerCase())
  )
}

export { proccesAsset, matchQuery }
