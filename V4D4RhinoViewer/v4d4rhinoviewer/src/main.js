import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import store from './store'
import { EventBus } from './eventbus'
import VueObserveVisibility from 'vue-observe-visibility'
Vue.use(VueObserveVisibility)

Vue.config.productionTip = false
Vue.config.performance = true

window.Store = store

new Vue({
  store,
  render: h => h(App),
  mounted () {
    EventBus.$on('add-asset', (assetData) => {
      // console.log(JSON.parse(assetData))
      this.$store.commit('ADD_ASSET', assetData)
    })
    EventBus.$on('init-crawl', (crawlResponse) => {
      // console.log('initiated crawl from .net ' + crawlResponse)
      this.$store.commit('CRAWL_STATE', crawlResponse)
    })
    EventBus.$on('query-result', (queryResult) => {
      this.$store.commit('QUERY_RESULT', queryResult)
    })
    EventBus.$on('clear-assets', (data) => {
      this.$store.commit('CLEAR_ASSETS', data)
    })
    EventBus.$on('download-progress-update', (data) => {
      // console.log('Download Progress: ' + data)
      this.$store.commit('UPDATE_DOWNLOAD_PROGRESS', data)
    })
    EventBus.$on('download-complete', (data) => {
      // console.log('Download Progress: ' + data)
      this.$store.commit('DOWNLOAD_COMPLETE', data)
    })
    EventBus.$on('model-added', (data) => {
      this.$store.commit('MODEL_ADDED', data)
    })
    EventBus.$on('modify-asset', (data) => {
      this.$store.commit('MODIFY_ASSET', data)
    })
    EventBus.$on('app-version', (data) => {
      this.$store.commit('SET_VERSION', data)
      console.log(this.$store.getters.version)
    })
  }
}).$mount('#app')

window.EventBus = EventBus // expose globally for .net
