import Vue from 'vue'
import Vuex from 'vuex'
import { EventBus } from './eventbus'
// import { matchQuery } from './modules/ProcessAsset.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    assets: [],
    crawlState: '',
    tags: [],
    localAssets: [],
    filteredAssets: [],
    bookmarkedAssets: [],
    assetOptions: [],
    hasOptions: false,
    textQuery: '',
    cleared: false,
    version: ''
  },
  getters: {
    assets: state => state.assets,
    crawlState: state => state.crawlState,
    tags: state => state.tags,
    localAssets: state => state.localAssets,
    filteredAssets: state => state.filteredAssets,
    bookmarkedAssets: state => state.bookmarkedAssets,
    assetOptions: state => state.assetOptions,
    textQuery: state => state.textQuery,
    hasOptions: state => state.hasOptions,
    cleared: state => state.cleared,
    version: state => state.version
  },
  mutations: {
    ADD_ASSET (state, data) {
      let asset = null

      try {
        asset = JSON.parse(data)
        if (asset.downloadUrl !== null) asset.downloadUrl = atob(asset.downloadUrl)
        // if (asset.description !== null) console.log(asset.description)
      } catch (e) {
        console.log(e)
      }

      // eslint-disable-next-line
      // var img = Interop.resizeImg(asset.thumbnailUrl, 150, 150)
      // console.log(img)

      // console.log(asset)
      state.assets.push(Object.freeze(asset))
    },
    SET_VERSION (state, data) {
      state.version = data
    },
    ADD_BOOKMARKED_ASSET (state, data) {
      console.log('bookmarking')
      console.log(state.bookmarkedAssets.length)
      if (state.bookmarkedAssets.includes(data)) return
      state.bookmarkedAssets.push(data)
      // eslint-disable-next-line
      // Interop.addBookmarkedAsset(data)
    },
    FILTER_TEXT (state, data) {
      state.cleared = false
      state.textQuery = data.text.toLowerCase()
      let results = []

      if (state.textQuery.length > 0) {
        results = state.assets.filter(item =>
          item.assetId.toLowerCase().includes(state.textQuery) ||
          item.name.toLowerCase().includes(state.textQuery) ||
          item.description.map(a => `(${Object.values(a)})`).join('').toLowerCase().includes(state.textQuery) ||
          item.tags.join(' ').toLowerCase().includes(state.textQuery)
        )
      } else {
        state.filteredAssets = state.assets
        return
      }

      if (results.length > 0) {
        state.filteredAssets = results
      } else {
        state.filteredAssets = []
      }
    },
    FILTER_OPTIONS (state, data) {
      state.cleared = false
      // state.assetOptions = data.options

      console.log(state.filteredAssets.length)
      this.commit('FILTER_TEXT', { text: state.textQuery })
      console.log(state.filteredAssets.length)

      let options = data.options
      let filtered = []
      let results = state.filteredAssets

      if (options === null && state.hasOptions) {
        options = state.assetOptions
      } else {
        state.assetOptions = data.options
      }

      // reset
      if (!options.bookmarkedSwitch && !options.fullSwitch && !options.basicSwitch && !options.downloadedSwitch && !options.bimSwitch) {
        state.hasOptions = false
        state.filteredAssets = results
        return
      } else {
        state.hasOptions = true
      }

      for (var i = 0; i < results.length; i++) {
        const asset = results[i]
        if (options.bookmarkedSwitch && state.bookmarkedAssets.length > 0) {
          if (state.bookmarkedAssets.includes(asset) && !filtered.includes(asset)) {
            filtered.push(asset)
            // continue
          }
        }
        if (options.fullSwitch) {
          if (asset.downloadUrl !== null && !filtered.includes(asset)) {
            filtered.push(asset)
            // continue
          }
        }
        if (options.basicSwitch) {
          if (asset.basicAssets === null && !filtered.includes(asset)) {
            filtered.push(asset)
            // continue
          }
        }
        if (options.downloadedSwitch) {
          if (asset.downloadUrl.includes('V4Design') && !filtered.includes(asset)) {
            filtered.push(asset)
            // continue
          }
        }
        if (options.bimSwitch) {
          if (asset.bim.length > 0 && !filtered.includes(asset)) {
            filtered.push(asset)
            // continue
          }
        }
      }

      console.log(filtered.length)
      if (filtered.length === 0) {
        state.filteredAssets = []
      } else {
        state.filteredAssets = filtered
      }
    },
    GET_LATEST (state) {
      this.commit('CLEAR_ASSETS', state)
      // eslint-disable-next-line
      Interop.getLatest( {page: 0, count: 150, assetType: null} )
    },
    CRAWL_STATE (state, data) {
      // console.log('initiated crawl from .net ' + data)
      state.crawlState = data
      EventBus.$emit('CRAWL_RESULT', data)
    },
    CLEAR_ASSETS (state) {
      state.assets = []
      state.tags = []
      state.filteredAssets = []
      state.bookmarkedAssets = []
      EventBus.$emit('CLEAR_OPTIONS', null)
      state.cleared = true
      /*
      if (state.bookmarkedAssets.length > 0) {
        state.bookmarkedAssets.forEach((a) => {
          state.assets.push(a)
        })
      }
      */
    },
    ADD_LOCAL_ASSET (state, data) {
      if (state.localAssets.includes(data)) return
      state.localAssets.push(data)
    },
    UPDATE_DOWNLOAD_PROGRESS (state, data) {
      EventBus.$emit('DOWNLOAD_PROGRESS', JSON.parse(data))
    },
    DOWNLOAD_COMPLETE (state, data) {
      EventBus.$emit('UPDATE_DOWNLOAD', data)
    },
    MODEL_ADDED (state, data) {
      EventBus.$emit('MODEL_IN_RHINO', data)
    },
    MODIFY_ASSET (state, data) {
      // state.assets.find(a => a.id === data.id)
      let d = JSON.parse(data)
      console.log(d)
      let a = state.assets.find(a => a.id === d.Id)
      console.log(a)

      switch (d.Property) {
        case 'RhinoObjectIds':
          if (a.rhinoObjectIds === null) {
            a.rhinoObjectIds = []
          }
          d.Value.forEach(id => {
            a.rhinoObjectIds.push(id)
          })
          break
      }

      console.log(a)
    },
    async SEARCH_V4D (state, data) {
      this.commit('CLEAR_ASSETS', state)
      EventBus.$emit('SEARCH_STARTED')
      // eslint-disable-next-line
      const success = await Interop.search(data)
      console.log(success)
      if (!success) {
        EventBus.$emit('SEARCH_FAILED')
      }
    }
  },
  actions: {
  }
})
