module.exports = {
  devServer: {
    port: 7070
  },
  baseUrl: process.env.NODE_ENV === 'production'
           ? '././'
            : '',
  outputDir: 'app'
  
}
